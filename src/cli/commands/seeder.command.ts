import { Command, Positional } from 'nestjs-command';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { UserService } from '../../user/user.service';
import { RfidService } from '../../rfid/rfid.service';
import { AuthService } from '../../auth/auth.service';
import { RandomGenerator } from 'typeorm/util/RandomGenerator';
import { ToolService } from '../../tool/tool.service';
import { ConfigService } from '../../config/config.service';
import { BookingService } from '../../booking/booking.service';
import { EventService } from '../../event/event.service';
import { OrganizationService } from '../../organization/services/organization.service';
import { SpaceService } from '../../organization/services/space.service';
import { RecurringTypes } from '../../event/enums/recurring.enum';
import { HubService } from '../../organization/services/hub.service';
import { Repository } from 'typeorm';
import { WorkingHours } from '../../organization/entity/working.hours.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class SeederCommand {
  constructor(
    @Inject('OrganizationService')
    private readonly organizationService: OrganizationService,
    @Inject('HubService')
    private readonly hubService: HubService,
    @Inject('SpaceService')
    private readonly spaceService: SpaceService,
    @Inject('RfidService')
    private readonly rfidService: RfidService,
    @Inject('UserService')
    private readonly userService: UserService,
    @Inject('AuthService')
    private readonly authService: AuthService,
    @Inject('ToolService')
    private readonly toolService: ToolService,
    @Inject('BookingService')
    private readonly bookingService: BookingService,
    @Inject('EventService')
    private readonly eventService: EventService,
    @Inject('ConfigService')
    private readonly config: ConfigService,
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
  ) {}

  @Command({ command: 'seeder:all <keyword>', describe: 'Seeds database' })
  async create(
    @Positional({
      name: 'keyword',
    })
    keyword: string,
  ) {
    const uniqnum = Math.floor((Math.random() + 1) * 100000),
      uniqstr = `${keyword} ${uniqnum}`;
    const organization = await this.organizationService.create({
      title: `Test organization ${uniqstr}`,
    });
    const hub = await this.hubService.create({
      title: `Test hub ${uniqstr}`,
      organization,
    });
    const space = await this.spaceService.create({
      title: `Test space ${uniqstr}`,
      description: `Test space description ${uniqstr}`,
      hubId: hub.id,
      workingHours: null,
    });
    const controller = await this.authService.findOneOrCreateNewController({
      id: RandomGenerator.uuid4(),
      space,
    });
    const toolTemplate = await this.toolService.createTemplate({
      title: `Test template ${uniqstr}`,
      description: `Test template description ${uniqstr}`,
      minWorkTime: 6,
      maxWorkTime: 20,
      buddyMode: true,
      availableInWorkingHours: true,
      requiresTraining: true,
      organizationId: organization.id,
      sound: true,
      tools: [],
    });
    const tools = await this.toolService.createTools([
      {
        id: RandomGenerator.uuid4(),
        title: `Test tool ${uniqstr}`,
        template: toolTemplate,
        controller,
      },
      {
        id: RandomGenerator.uuid4(),
        title: `Test tool ${uniqstr}_2`,
        template: toolTemplate,
        controller,
      },
    ]);
    const useremail = `testemail${uniqnum}@test.com`,
      userpass = String(uniqnum),
      user = await this.userService.create(
        {
          rfidUID: `cardid${uniqnum}`,
          firstName: 'Test first name',
          lastName: 'Test last name',
          email: useremail,
          description: 'Test description',
          organizationId: organization.id,
          password: userpass,
          photo: null,
        },
        false,
      );
    user.toolTemplates = [toolTemplate];
    await this.userService.activate(user);
    await this.workingHoursRepo.save([
      {
        day: 2,
        isWorking: true,
        from: 0,
        to: 1000,
        space,
        user,
      },
    ]);

    const nextTuesday = new Date();
    nextTuesday.setDate(
      nextTuesday.getDate() + ((2 + 7 - nextTuesday.getDay()) % 7),
    );
    await Promise.all([
      this.bookingService.create({
        timeSlot: { from: 10, to: 12 },
        ownerId: user.id,
        toolId: tools[0].id,
        bookingDate: nextTuesday,
        buddyEmail: null,
      }),
      this.eventService.create({
        title: 'Test event',
        eventDate: nextTuesday,
        timeSlot: { from: 14, to: 20 },
        spaceId: space.id,
        toolIds: [tools[0].id],
        recurring: RecurringTypes.ONE_TIME,
      }),
      this.authService.createOrUpdateToken(user, true),
    ]);

    Logger.log(
      `User has been created. To log in please use next credentials: login: '${useremail}', password: '${userpass}'`,
    );
  }
}
