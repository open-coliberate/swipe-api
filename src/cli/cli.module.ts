import { Module } from '@nestjs/common';
import { SeederCommand } from './commands/seeder.command';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organization } from '../organization/entity/organization.entity';
import { Hub } from '../organization/entity/hub.entity';
import { Space } from '../organization/entity/space.entity';
import { UserService } from '../user/user.service';
import { RfidService } from '../rfid/rfid.service';
import { User } from '../user/entity/user.entity';
import { RFIDCard } from '../rfid/entity/rfid.card.entity';
import { ResetPasswordRequest } from '../auth/entity/reset.password.request.entity';
import { Tool } from '../tool/entity/tool.entity';
import { ToolTemplate } from '../tool/entity/tool.template.entity';
import { MailerService } from '../mailer/mailer.service';
import { RFIDController } from '../rfid/entity/rfid.controller.entity';
import { AuthService } from '../auth/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { PassportModule } from '@nestjs/passport';
import { ToolService } from '../tool/tool.service';
import { AuthToken } from '../auth/entity/auth.token.entity';
import { BookingService } from '../booking/booking.service';
import { Booking } from '../booking/booking.entity';
import { EventService } from '../event/event.service';
import { Event } from '../event/event.entity';
import { OrganizationService } from '../organization/services/organization.service';
import { SpaceService } from '../organization/services/space.service';
import { HubService } from '../organization/services/hub.service';
import { DateTimeService } from '../services/date.time.service';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.get('APP_SECRET_KEY'),
        signOptions: {
          expiresIn: configService.get('APP_TOKEN_EXPIRES_IN'),
        },
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([
      Organization,
      Hub,
      Space,
      User,
      RFIDController,
      RFIDCard,
      AuthToken,
      ResetPasswordRequest,
      Tool,
      ToolTemplate,
      Booking,
      Event,
      WorkingHours,
    ]),
    ConfigModule,
  ],
  providers: [
    OrganizationService,
    HubService,
    SpaceService,
    UserService,
    RfidService,
    SeederCommand,
    MailerService,
    AuthService,
    ToolService,
    BookingService,
    EventService,
    DateTimeService,
  ],
  exports: [],
})
export class CliModule {}
