import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerService } from '../mailer/mailer.service';
import { ApplicationMailerModule } from '../mailer/mailer.module';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { RFIDController } from '../rfid/entity/rfid.controller.entity';
import { JwtControllerStrategy } from './strategies/jwt.controller.strategy';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';
import { ResetPasswordRequest } from './entity/reset.password.request.entity';
import { Hub } from '../organization/entity/hub.entity';
import { Space } from '../organization/entity/space.entity';
import { AuthToken } from './entity/auth.token.entity';
import { User } from '../user/entity/user.entity';
import { HubService } from '../organization/services/hub.service';
import { OrganizationService } from '../organization/services/organization.service';
import { SpaceService } from '../organization/services/space.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      ResetPasswordRequest,
      RFIDController,
      Hub,
      Space,
      AuthToken,
    ]),
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.get('APP_SECRET_KEY'),
        signOptions: {
          expiresIn: configService.get('APP_TOKEN_EXPIRES_IN'),
        },
      }),
      inject: [ConfigService],
    }),
    ApplicationMailerModule,
    UserModule,
  ],
  providers: [
    AuthService,
    JwtStrategy,
    JwtControllerStrategy,
    MailerService,
    UserService,
    HubService,
    OrganizationService,
    SpaceService,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
