import {
  BadRequestException,
  Body,
  Controller,
  FileInterceptor,
  ForbiddenException,
  HttpStatus,
  Post,
  Put,
  Res,
  UnauthorizedException,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitFile,
  ApiResponse,
  ApiUseTags,
  ApiOperation,
} from '@nestjs/swagger';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { MailerService } from '../mailer/mailer.service';
import { MessageType } from '../mailer/constants/mailer.constants';
import { RfidService } from '../rfid/rfid.service';
import { CurrentUser } from './decorators/current.user.decorator';
import { User } from '../user/entity/user.entity';
import { Hub } from '../organization/entity/hub.entity';
import { HubService } from '../organization/services/hub.service';
import { SpaceService } from '../organization/services/space.service';
import { BaseResponse } from '../domain/responses/base.response';
import { UserWithRFIDCardDto } from '../user/dto/user.with.RFIDCard.dto';
import { UserBaseDto } from '../user/dto/user.base.dto';
import { UserSignInDto } from '../user/dto/user.signin.dto';
import { RefreshTokenDto } from './dto/refresh.token.dto';
import { ResetPasswordRequestDto } from './dto/reset.password.request.dto';
import {
  ResetPasswordWithOldPasswordDto,
  ResetPasswordWithTokenDto,
} from './dto/reset.password.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { ControllerGetInitInfoDto } from './dto/controller.get.init.info.dto';
import { UserNotActiveException } from '../user/exceptions/user.not.active.exception';
import { UserResponseWithToken } from '../user/responses/user.response.with.token';
import { ValidationFailedResponse } from '../user/responses/validation.failed.response';
import { RfidControllerInitInNewHubConfirmation } from '../rfid/dto/rfid.controller.init.in.new.hub.confirmation';

@Controller('auth')
@ApiUseTags('auth')
export class AuthController {
  constructor(
    private readonly usersService: UserService,
    private readonly authService: AuthService,
    private readonly mailerService: MailerService,
    private readonly rfidService: RfidService,
    private readonly hubService: HubService,
    private readonly spaceService: SpaceService,
  ) {}

  @Post('sign-up')
  @ApiOperation({ title: 'Register a new user' })
  @ApiResponse({ status: HttpStatus.CREATED, type: UserResponseWithToken })
  @ApiResponse({
    status: HttpStatus.NOT_ACCEPTABLE,
    description: 'Validation failed',
    type: ValidationFailedResponse,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  async signUp(@Body() userWithRFIDCardDto: UserWithRFIDCardDto) {
    try {
      const user = await this.usersService.create(userWithRFIDCardDto);
      return await this.authService.createOrUpdateToken(user, true);
    } catch (error) {
      throw error;
    }
  }

  @Post('sign-in')
  @ApiOperation({ title: 'User login' })
  @ApiResponse({ status: HttpStatus.OK, type: UserResponseWithToken })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Invalid password',
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Account is not activated yet',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  async signIn(@Body() userSignInDto: UserSignInDto) {
    const user = await this.usersService.findOneByEmail(userSignInDto.email);
    if (!user) {
      throw new UnauthorizedException('Login failed, invalid user');
    }
    const isPasswordCorrect = await this.authService.validateUserPassword(
      user,
      userSignInDto.password,
    );
    if (!isPasswordCorrect) {
      throw new UnauthorizedException('Invalid password');
    }
    if (!user.isActive) {
      throw new UserNotActiveException(
        'Account is not activated yet, please activate them!',
      );
    }
    // We should return token only after sign-in
    const {
      isActive,
      password,
      ...updatedUser
    } = await this.authService.createOrUpdateToken(user);
    return {
      ...updatedUser,
    };
  }

  @Post('refresh-token')
  @ApiOperation({ title: 'Refresh access token using refresh token.' })
  @ApiResponse({ status: HttpStatus.CREATED, type: UserResponseWithToken })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Invalid token',
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Account is not activated yet',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({ status: HttpStatus.CREATED, type: UserBaseDto })
  async refreshToken(@Body() refreshTokenDto: RefreshTokenDto) {
    const user = await this.usersService.findOneByRefreshToken(
      refreshTokenDto.refreshToken,
    );
    if (!user) {
      throw new UnauthorizedException('Invalid token');
    }
    if (!user.isActive) {
      throw new UserNotActiveException(
        'Account is not active, please activate them!',
      );
    }

    // Just skip unnecessary fields
    const {
      isActive,
      password,
      ...updatedUser
    } = await this.authService.createOrUpdateToken(user);
    return {
      ...updatedUser,
    };
  }

  @Put('reset-password-request')
  @ApiOperation({
    title: 'Creates reset password request and send it to user email',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description:
      'Password reset request has been created. Please check your mailbox for further details.',
  })
  @ApiResponse({
    status: HttpStatus.NOT_ACCEPTABLE,
    description: 'Validation failed',
    type: ValidationFailedResponse,
  })
  async resetPasswordRequest(
    @Body() resetPasswordDto: ResetPasswordRequestDto,
  ) {
    const user = await this.usersService.findOneByEmail(resetPasswordDto.email);
    if (!user) {
      throw new UnauthorizedException('Invalid payload');
    }
    user.resetPasswordRequest = await this.authService.generatePasswordResetRequest();
    await this.usersService.save(user);

    const mailerMessage = this.mailerService.getMessage(
      user.email,
      MessageType.ResetPassword,
      {
        firstName: user.firstName,
        lastName: user.lastName,
        token: user.resetPasswordRequest.token,
        hostName: process.env.APP_HOST,
      },
    );
    await this.mailerService.addMessageForProcessing(mailerMessage);
    return {
      message:
        'Password reset request has been created. Please check your mailbox for further details.',
      errors: [],
    };
  }

  @Put('reset-password')
  @ApiOperation({
    title: 'Reset user password using, pre-generated secret token',
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description:
      'You cannot reset your password without valid reset password token',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({ status: HttpStatus.OK, type: BaseResponse })
  async resetPassword(@Body() resetPassword: ResetPasswordWithTokenDto) {
    const user = await this.usersService.findOneByPasswordResetToken(
      resetPassword.token,
    );
    if (!user) {
      throw new ForbiddenException(
        'You cannot reset your password without valid reset password token!',
      );
    }
    return await this.authService.resetPassword(user, resetPassword);
  }

  @Put('password')
  @ApiOperation({
    title: 'Reset user password without token',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async resetPasswordAuthorised(
    @Body() payload: ResetPasswordWithOldPasswordDto,
    @CurrentUser() user: User,
  ) {
    if (
      !(await this.authService.validateUserPassword(user, payload.oldPassword))
    ) {
      throw new ForbiddenException('Wrong old password');
    }
    return this.authService.resetPassword(user, payload);
  }

  @Post('/controller/init-send-email')
  @ApiOperation({
    title: 'First step of controller initialization',
    description:
      'Server sends security code to user and retuns list of allowed hubs',
  })
  @ApiResponse({ status: HttpStatus.OK, type: [Hub] })
  async controllerInitSendEmail(@Body() payload: ControllerGetInitInfoDto) {
    const [labManager, controller] = await Promise.all([
      this.usersService.findOneByEmail(payload.labManagerEmail),
      this.authService.findOneOrCreateNewController({
        id: payload.deviceId,
        space: null,
      }),
    ]);

    const secureCode = Math.floor(Math.random() * 10e5) + 10e4,
      mailerMessage = this.mailerService.getMessage(
        labManager.email,
        MessageType.InitInNewHub,
        {
          firstName: labManager.firstName,
          lastName: labManager.lastName,
          controllerName: payload.nickname,
          secureCode,
        },
      );

    const [, , hubs] = await Promise.all([
      this.rfidService.setInitKeyToController(controller, secureCode),
      this.mailerService.addMessageForProcessing(mailerMessage),
      this.hubService.findByOrganizationTitle(payload.organizationTitle),
    ]);
    return hubs;
  }

  @Post('/controller/init-confirmation')
  @ApiOperation({
    title: 'Second step of controller initialization',
    description:
      'Server checks all input data and returns access tokens of controller',
  })
  @ApiResponse({ status: HttpStatus.ACCEPTED })
  async controllerInitInNewHubConfirmation(
    @Body() payload: RfidControllerInitInNewHubConfirmation,
  ) {
    const hub = await this.hubService.findOneById(payload.hubId);
    const [controller, space] = await Promise.all([
      this.rfidService.findOneController(payload.deviceId, {
        relations: ['token'],
      }),
      this.spaceService.getDefaultOne(hub),
    ]);
    controller.space = space;

    await this.rfidService.setInitKeyToController(controller, null);
    return controller;
  }

  @Post('/controller/refresh-token')
  @ApiResponse({ status: HttpStatus.ACCEPTED })
  async controllerRefreshToken(@Body() refreshTokenDto: RefreshTokenDto) {
    const rfidController = await this.rfidService.findOneControllerByRefreshToken(
      refreshTokenDto.refreshToken,
    );
    if (!rfidController) {
      throw new UnauthorizedException('Invalid token');
    }
    const { accessToken } = await this.authService.createToken({
      id: rfidController.id,
    });
    return await this.rfidService.updateControllerAccessToken(
      rfidController,
      accessToken,
    );
  }

  @Post('sign-up/csv')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiOperation({
    title: 'Csv file with users which will be created',
  })
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'File columns firstName, lastName, email, rfidUID.',
  })
  @UseInterceptors(
    FileInterceptor('file', {
      limits: { files: 1, fileSize: 2000000 },
      fileFilter: (req, file, callback) => {
        if (file.mimetype !== 'text/csv') {
          callback(new BadRequestException('Only csv files allowed'), false);
        }
        callback(null, true);
      },
    }),
  )
  async saveUsersCsv(
    @UploadedFile('file') file,
    @Res() res,
    @CurrentUser() user: User,
  ) {
    const organizationId = user.organization ? user.organization.id : null;
    this.usersService.saveCsv(
      file.buffer.toString(),
      organizationId,
      (countItems, errors) => {
        res.status(HttpStatus.OK).json({
          status: errors.length === 0,
          errors,
          succeed: countItems - errors.length,
        });
      },
      reason => {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          status: false,
          message: reason,
        });
      },
    );
  }
}
