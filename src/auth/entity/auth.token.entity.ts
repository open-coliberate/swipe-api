import { Column, Entity } from 'typeorm';
import { TokenDto } from '../dto/token.dto';
import { CoreEntity } from '../../domain/entity/core.entity';

@Entity('tokens')
export class AuthToken extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  accessToken: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  refreshToken: string;

  static fromDto(dto: TokenDto): AuthToken {
    const token = new AuthToken({});
    for (const [key, value] of Object.entries(dto)) token[key] = value;
    return token;
  }

  static toDto(token: AuthToken): TokenDto {
    const dto = new TokenDto();
    for (const [key, value] of Object.entries(token)) dto[key] = value;

    return dto;
  }
}
