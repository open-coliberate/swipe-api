import { Column, Entity } from 'typeorm';
import { Exclude } from 'class-transformer';
import { CoreEntity } from '../../domain/entity/core.entity';
@Entity('reset_password_requests')
@Exclude()
export class ResetPasswordRequest extends CoreEntity {
  @Column({
    type: 'varchar',
  })
  token: string;
}
