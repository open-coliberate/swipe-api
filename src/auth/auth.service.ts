import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import nanoid from 'nanoid/async';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { RFIDController } from '../rfid/entity/rfid.controller.entity';
import { AuthToken } from './entity/auth.token.entity';
import { RfidControllerDto } from '../rfid/dto/rfid.controller.dto';
import { ResetPasswordRequest } from './entity/reset.password.request.entity';
import { User } from '../user/entity/user.entity';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { JwtControllerPayload } from './interfaces/jwt-controller-payload.interface';
import {
  BaseResetPasswordDto,
  ResetPasswordWithTokenDto,
} from './dto/reset.password.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(ResetPasswordRequest)
    private readonly passwordResetRepository: Repository<ResetPasswordRequest>,
    @InjectRepository(AuthToken)
    private readonly authTokenRepo: Repository<AuthToken>,
    private readonly usersService: UserService,
    @InjectRepository(RFIDController)
    private readonly rfidControllerRepository: Repository<RFIDController>,
    private readonly jwtService: JwtService,
  ) {}

  async createToken(jwtPayload: JwtPayload | JwtControllerPayload) {
    const accessToken = await this.jwtService.signAsync(jwtPayload, {
      expiresIn: process.env.APP_TOKEN_EXPIRES_IN,
    });
    return {
      accessToken,
      refreshToken: await nanoid(256),
    };
  }

  async resetPassword(user: User, resetPasswordDto: BaseResetPasswordDto) {
    if (resetPasswordDto.password !== resetPasswordDto.passwordConfirmation) {
      throw new ForbiddenException(
        `Password and ConfirmationPassword don't match`,
      );
    }
    user.password = resetPasswordDto.password;
    if (user.resetPasswordRequest) {
      await this.passwordResetRepository.remove(user.resetPasswordRequest);
      user.resetPasswordRequest = null;
    }
    return await this.createOrUpdateToken(user, true);
  }

  async createOrUpdateToken(
    user: User,
    updateRefresh: boolean = false,
  ): Promise<User> {
    const authToken = await this.createToken({
      email: user.email,
    });

    const { accessToken, refreshToken } = authToken;
    if (!user.token) {
      user.token = await this.authTokenRepo.create();
    }
    user.token.accessToken = accessToken;
    if (updateRefresh) {
      user.token.refreshToken = refreshToken;
    }

    return await this.usersService.save(user);
  }

  async refreshToken(refreshToken: string): Promise<string> {
    const user = await this.usersService.findOneByRefreshToken(refreshToken);
    if (!user) {
      throw new NotFoundException();
    }

    const { accessToken } = await this.createToken({ email: user.email });
    user.token.accessToken = accessToken;
    await this.usersService.save(user);

    return user.token.accessToken;
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    return await this.usersService.findOneByEmail(payload.email);
  }

  async validateUserPassword(user: User, password: string): Promise<boolean> {
    if (!user.password) return false;
    return await compare(password, user.password);
  }

  async generatePasswordResetRequest() {
    return await this.passwordResetRepository.save(
      await this.passwordResetRepository.create({
        token: await nanoid(128),
      }),
    );
  }

  async findOneOrCreateNewController(
    payload: RfidControllerDto,
  ): Promise<RFIDController> {
    try {
      return await this.rfidControllerRepository.findOneOrFail(payload.id);
    } catch (e) {
      if (e.name === 'EntityNotFound') {
        const instance = this.rfidControllerRepository.create(payload);
        instance.token = AuthToken.fromDto(await this.createToken(payload));
        return this.rfidControllerRepository.save(instance);
      }
      throw e;
    }
  }

  async validateController(payload: JwtControllerPayload) {
    return await this.rfidControllerRepository.findOne(payload.id);
  }
}
