import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { JwtControllerPayload } from '../interfaces/jwt-controller-payload.interface';

@Injectable()
export class JwtControllerStrategy extends PassportStrategy(
  Strategy,
  'jwtController',
) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.APP_SECRET_KEY,
      passReqToCallback: true,
    });
  }

  async validate(req, payload: JwtControllerPayload) {
    if (!payload.id) {
      throw new UnauthorizedException('Wrong payload format');
    }
    const rfidController = await this.authService.validateController(payload);
    if (!rfidController) {
      throw new UnauthorizedException('Access denied');
    }

    return rfidController;
  }
}
