import { ReflectMetadata } from '@nestjs/common';

export const OnlyRoles = (...roles: string[]) =>
  ReflectMetadata('roles', roles);
