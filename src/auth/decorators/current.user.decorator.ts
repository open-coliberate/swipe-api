import { createParamDecorator } from '@nestjs/common';

export const CurrentUser = createParamDecorator((data, request) => {
  return request.user;
});
