import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtControllerGuard extends AuthGuard('jwtController') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }
  handleRequest(err, controller, info) {
    if (err || !controller) {
      throw err ||
        new UnauthorizedException('Access token is invalid or expired');
    }
    return controller;
  }
}
