import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserNotActiveException } from '../../user/exceptions/user.not.active.exception';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }
  handleRequest(err, user, info) {
    if (err || !user) {
      throw err ||
        new UnauthorizedException('Access token is invalid or expired');
    }

    if (!user.isActive) {
      throw new UserNotActiveException(
        'User is not active. Activate them before any actions will be performed',
      );
    }

    return user;
  }
}
