import {
  CanActivate,
  ExecutionContext,
  Injectable,
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserService } from '../../user/user.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly userService: UserService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    // If roles decorator isn't exist - just allow access.
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers.authorization;
    // If no Bearer token present - forbidden!
    if (!authHeader) {
      return false;
    }
    const token = authHeader.slice('Bearer '.length).trim();
    const user = await this.userService.findOneByAccessToken(token);
    if (!user) {
      throw new UnauthorizedException('Access token is invalid or expired');
    }

    const hasRole = () => roles.includes(user.role);
    const isAccessByRolesGranted = user && user.role && hasRole();
    if (!isAccessByRolesGranted) {
      throw new ForbiddenException(
        `You should be promoted to following groups: ${roles.join(
          ',',
        )} to be able to proceed with this request!`,
      );
    }
    return isAccessByRolesGranted;
  }
}
