import { Equals, IsNotEmpty, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class BaseResetPasswordDto {
  @ApiModelProperty({
    description: 'A new password',
    required: true,
  })
  @IsNotEmpty()
  @MinLength(6)
  password: string;

  @ApiModelProperty({
    description: 'A new password confirmation',
    required: true,
  })
  @IsNotEmpty()
  @MinLength(6)
  passwordConfirmation: string;
}

export class ResetPasswordWithTokenDto extends BaseResetPasswordDto {
  @ApiModelProperty({
    description: 'Password reset request token',
    required: false,
  })
  @IsNotEmpty()
  token: string;
}

export class ResetPasswordWithOldPasswordDto extends BaseResetPasswordDto {
  @ApiModelProperty({
    description: 'Old password',
    required: true,
  })
  @MinLength(6)
  @IsNotEmpty()
  oldPassword: string;
}
