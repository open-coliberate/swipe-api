import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetPasswordRequestDto {
  @ApiModelProperty({
    description: 'User email',
    required: true,
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
