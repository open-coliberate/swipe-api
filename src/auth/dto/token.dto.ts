import { ApiModelProperty } from '@nestjs/swagger';

export class TokenDto {
  @ApiModelProperty({
    type: 'string',
    description: 'Access Token',
    required: true,
  })
  accessToken: string;

  @ApiModelProperty({
    type: 'string',
    description: 'Refresh Token',
    required: true,
  })
  refreshToken: string;
}
