import { IsNotEmpty, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdatePasswordDto {
  @ApiModelProperty({
    description: 'A new password',
    required: true,
  })
  @IsNotEmpty()
  @MinLength(6)
  password: string;

  @ApiModelProperty({
    description: 'A new password confirmation',
    required: true,
  })
  @IsNotEmpty()
  @MinLength(6)
  passwordConfirmation: string;
}
