import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { IsUserLabManagerByOrganizationTitle } from '../../user/validators/controller.init.flow';

export class ControllerGetInitInfoDto {
  @ApiModelProperty({
    description: 'String with organization name',
    required: true,
  })
  @IsNotEmpty()
  organizationTitle: string;

  @ApiModelProperty({
    description: `Lab manager's email`,
    required: true,
    format: 'email',
  })
  @IsNotEmpty()
  @IsEmail()
  @IsUserLabManagerByOrganizationTitle()
  labManagerEmail: string;

  @ApiModelProperty({
    description: 'Card code of the lab manager',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  RFIDCardCode: string;

  @ApiModelProperty({
    description: 'Id of the controller',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  deviceId: string;

  @ApiModelProperty({
    description: 'Nickname of the device',
    required: false,
  })
  @IsString()
  @IsOptional()
  nickname: string;
}
