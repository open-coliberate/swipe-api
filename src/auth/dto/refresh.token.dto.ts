import { ApiModelProperty } from '@nestjs/swagger';

export class RefreshTokenDto {
  @ApiModelProperty({
    description: 'Refresh Token',
    required: true,
  })
  refreshToken: string;
}