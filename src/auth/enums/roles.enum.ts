export enum Roles {
  OWNER = 'owner',
  SUPER_USER = 'super-user',
  GENERAL_USER = 'general',
  LAB_MANAGER = 'lab-manager',
}
