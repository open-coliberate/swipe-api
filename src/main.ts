import { ApplicationContext } from './application/app.context';

async function bootstrap() {
  const app = await ApplicationContext();
  await app.listen(process.env.PORT || process.env.APP_PORT);
}

bootstrap();
