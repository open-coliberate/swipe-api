import * as Joi from 'joi';

export const applicationConfigSchema: Joi.ObjectSchema = Joi.object({
  APP_HOST: Joi.string().default('api.coliberate.bbapps.io'),
  APP_ENV: Joi.string()
    .valid(['development', 'production', 'staging', 'test'])
    .default('development'),
  NODE_ENV: Joi.string()
    .valid(['development', 'production', 'staging', 'test'])
    .default('development'),
  APP_DEBUG: Joi.boolean().default(false),
  APP_PORT: Joi.number().default(3000),
  APP_SECRET_KEY: Joi.string(),
  APP_BASE_API_PATH: Joi.string(),
  APP_TOKEN_EXPIRES_IN: Joi.string().default('24h'),
  APP_CONTROLLER_INIT_TOKEN_EXPIRATION: Joi.number().default(300000),
  DATABASE_PORT: Joi.number().default(5432),
  DATABASE_HOST: Joi.string(),
  DATABASE_USER: Joi.string(),
  DATABASE_PASSWORD: Joi.string(),
  DATABASE_NAME: Joi.string(),
  REDIS_HOST: Joi.string(),
  REDIS_PORT: Joi.number().default(6379),
  SMTP_HOST: Joi.string(),
  SMTP_PORT: Joi.number().default(25),
  SMTP_USER: Joi.string(),
  SMTP_PASSWORD: Joi.string(),
  SMTP_FROM: Joi.string(),
  SMTP_ATTEMPTS: Joi.number().default(3),
  APP_TEMPLATES_DIRECTORY: Joi.string(),
  AWS_ACCESS_KEY: Joi.string(),
  AWS_SECRET_KEY: Joi.string(),
  AWS_S3_BUCKET_NAME: Joi.string(),
  ITEMS_PER_PAGE: Joi.number(),
});
