import { Injectable } from '@nestjs/common';
import { join } from 'path';
import { MailerModuleOptions } from '@nest-modules/mailer';
import { parse, config, DotenvParseOutput } from 'dotenv';
import * as Joi from 'joi';
import { applicationConfigSchema } from './config.schema';
import { Config } from './interfaces/config.interface';
import { FileSystemService } from '../services/file.system.service';

@Injectable()
export class ConfigService {
  static readonly DOT_ENV_FILE: string = '.env';
  static readonly ROOT_PATH: string = process.cwd();

  private readonly config: Config;
  constructor(private readonly fileSystemService: FileSystemService) {
    const dotEnvFilePath = join(
      ConfigService.ROOT_PATH,
      ConfigService.DOT_ENV_FILE,
    );
    if (this.fileSystemService.existsSync(dotEnvFilePath)) {
      const parsedConfig = parse(
        this.fileSystemService.readFileSync(dotEnvFilePath),
      );
      this.config = ConfigService.validateConfig(parsedConfig);
      config();
    }
  }

  get(key: string): string {
    return process.env[key] || this.config[key];
  }

  getMailerConfiguration(): MailerModuleOptions {
    return {
      transport: {
        host: this.get('SMTP_HOST'),
        port: Number(this.get('SMTP_PORT')),
        auth: {
          user: this.get('SMTP_USER'),
          pass: this.get('SMTP_PASSWORD'),
        },
      },
      defaults: {
        forceEmbeddedImages: true,
        from: this.get('SMTP_FROM'),
      },
      templateDir: this.get('APP_TEMPLATES_DIRECTORY'),
    };
  }

  private static validateConfig(
    parsedConfig: DotenvParseOutput,
  ): Config | DotenvParseOutput {
    const { error, value: validatedEnvConfig } = Joi.validate(
      parsedConfig,
      applicationConfigSchema,
    );
    if (error) {
      throw Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }
}
