import { Module, Global } from '@nestjs/common';
import { ConfigService } from './config.service';
import { FileSystemService } from '../services/file.system.service';

@Global()
@Module({
  providers: [ConfigService, FileSystemService],
  exports: [ConfigService],
})
export class ConfigModule {}
