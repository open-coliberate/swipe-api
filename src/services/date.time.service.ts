import { Injectable } from '@nestjs/common';
import { Moment } from 'moment';
import { MomentIntervalInterface } from '../domain/interfaces/interval.interface';
@Injectable()
export class DateTimeService {
  getWeekDaysBetweenDatesWithSteps(
    start: Moment,
    end: Moment,
    step: MomentIntervalInterface,
  ): Moment[] {
    const dates = [];
    start.startOf('day');
    end.startOf('day');
    const startDay = start.day();
    dates.push(start.clone());
    const workingDate = start.clone().day(startDay);
    while (workingDate.isBefore(end)) {
      workingDate.add(step.amount, step.units);
      dates.push(workingDate.clone());
    }
    return dates;
  }
}
