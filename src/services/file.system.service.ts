import * as fs from 'fs';
import { Injectable } from '@nestjs/common';
import { promisify } from 'util';
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdir = promisify(fs.mkdir);
const exists = promisify(fs.existsSync);

@Injectable()
export class FileSystemService {
  private readonly options = { encoding: 'UTF-8' };

  async readFileAsync(path) {
    return readFile(path, this.options);
  }

  async writeFileAsync(path, data) {
    await writeFile(path, data, this.options);
  }

  writeFileSync(path, data) {
    fs.writeFileSync(path, data, this.options);
  }
  readFileSync(path) {
    return fs.readFileSync(path, this.options);
  }
  async existsAsync(path) {
    return await exists(path);
  }

  existsSync(path) {
    return fs.existsSync(path);
  }

  async mkdirAsync(path, options) {
    return await mkdir(path, options);
  }
  mkdirSync(path, options) {
    return fs.mkdirSync(path, options);
  }
}
