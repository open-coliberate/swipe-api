import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import * as AWS from 'aws-sdk';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';

@Injectable()
export class StorageService {
  private storage: AWS.S3;
  constructor(private readonly configService: ConfigService) {
    AWS.config.update({
      accessKeyId: this.configService.get('AWS_ACCESS_KEY'),
      secretAccessKey: this.configService.get('AWS_SECRET_KEY'),
    });
    this.storage = new AWS.S3();
  }

  set(
    bucket: string,
    key: string,
    value: Buffer,
    contentType: string = 'application/octet-stream',
  ): Promise<ManagedUpload.SendData> {
    return this.storage
      .upload({
        Bucket: bucket,
        Key: key,
        Body: value,
        ACL: 'public-read',
        ContentType: contentType,
      })
      .promise();
  }
  remove(bucket: string, key: string) {
    return this.storage.deleteObject({ Bucket: bucket, Key: key }).promise();
  }

  async get(key) {}
}
