import { Injectable } from '@nestjs/common';
import sharp from 'sharp';
import { ConfigService } from '../config/config.service';
@Injectable()
export class ImageService {
  constructor(private readonly config: ConfigService) {}
  async resizeImage(inputFile): Promise<Buffer> {
    return await sharp(inputFile.buffer)
      .resize(120)
      .toBuffer();
  }
}
