import { BookingStatus, BookingStatuses } from '../booking.entity';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsUUID,
  ValidateIf,
} from 'class-validator';
import { IsTimeSlot } from '../validators/is.timeslot.validator';
import { TimeSlot } from '../../domain/interfaces/timeslot.interface';
import { ApiModelProperty } from '@nestjs/swagger';

export class BookingDTO {
  @ApiModelProperty({
    type: 'object',
    required: true,
  })
  @IsNotEmpty()
  @IsTimeSlot()
  timeSlot: TimeSlot;

  @ApiModelProperty({
    type: 'string',
    format: 'date-time',
    required: true,
  })
  @IsDateString()
  bookingDate: Date;

  @ApiModelProperty({
    type: 'string',
    required: true,
    enum: BookingStatuses,
  })
  @ValidateIf(entity => entity.eventStatus)
  @IsEnum(BookingStatuses)
  eventStatus?: BookingStatus;

  @ApiModelProperty({
    type: 'string',
    format: 'uuid',
    required: true,
  })
  @IsUUID()
  ownerId: string;

  @ApiModelProperty({
    type: 'string',
    format: 'uuid',
    required: true,
  })
  @IsUUID()
  toolId: string;

  @ApiModelProperty({
    type: 'string',
    format: 'email',
    required: true,
  })
  @ValidateIf(entity => entity.buddyEmail)
  @IsEmail()
  buddyEmail: string;
}
