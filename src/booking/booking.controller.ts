import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';
import { BookingService } from './booking.service';
import { Booking } from './booking.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { OnlyRoles } from '../auth/decorators/roles.decorator';
import { Roles } from '../auth/enums/roles.enum';
import { BookingDTO } from './dto/BookingDTO';
import { FindManyOptions } from 'typeorm';

@UseGuards(JwtAuthGuard)
@ApiUseTags('bookings')
@Controller('bookings')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

  @ApiOperation({
    title: 'Getting bookings',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER, Roles.SUPER_USER)
  @Get('/:date?')
  @ApiResponse({ status: HttpStatus.OK, type: [BookingDTO] })
  @ApiBearerAuth()
  async getAll(@Param('date') date: string) {
    const payload: FindManyOptions<Booking> = {
      relations: ['owner', 'tool'],
      where: undefined,
    };
    if (date) {
      payload.where = {
        bookingDate: date,
      };
    }
    return await this.bookingService.findAll(payload);
  }

  @ApiOperation({
    title: 'Creating new bookings',
  })
  @Post()
  @ApiResponse({ status: HttpStatus.OK, type: BookingDTO })
  @ApiBearerAuth()
  async create(@Body() bookingDTO: BookingDTO): Promise<Booking> {
    return await this.bookingService.create(bookingDTO);
  }
}
