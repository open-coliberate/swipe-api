import { CoreEntity } from '../domain/entity/core.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { User } from '../user/entity/user.entity';
import { Exclude, Expose, Transform } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';
import { Tool } from '../tool/entity/tool.entity';
import { TimeSlot } from '../domain/interfaces/timeslot.interface';
import { WorkingHours } from '../domain/interfaces/working.hours.interface';

export enum BookingStatuses {
  'PENDING' = 'pending',
  'IN_PROGRESS' = 'in progress',
  'DECLINED' = 'declined',
  'APPROVED' = 'approved',
  'FINISHED' = 'finished',
}
export type BookingStatus =
  | BookingStatuses.PENDING
  | BookingStatuses.IN_PROGRESS
  | BookingStatuses.DECLINED
  | BookingStatuses.APPROVED
  | BookingStatuses.FINISHED;

@Exclude()
@Entity('bookings')
export class Booking extends CoreEntity {
  @Expose()
  @Column({ name: 'time_slot', type: 'jsonb', nullable: false })
  timeSlot: TimeSlot;

  @Expose()
  @Transform((eventDate: Date) => {
    eventDate.setHours(0, 0, 0);
    return eventDate;
  })
  @Column({
    name: 'booking_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  bookingDate: Date;

  @Expose()
  @Column({
    type: 'varchar',
    default: BookingStatuses.PENDING,
    nullable: false,
  })
  status: BookingStatus = BookingStatuses.PENDING;

  @Expose()
  @OneToOne(() => User, {
    eager: true,
    nullable: true,
  })
  @JoinColumn()
  buddy?: User;

  @Expose()
  @Transform(owner => {
    return owner
      ? {
          id: owner.id,
          ownerFirstName: owner.firstName,
          ownerLastName: owner.lastName,
        }
      : null;
  })
  @ManyToOne(type => User, user => user.bookings, {
    cascade: false,
    eager: false,
    nullable: false,
  })
  owner: User;

  @ApiModelProperty({ readOnly: true, type: Tool })
  @Expose()
  @ManyToOne(type => Tool, tool => tool.bookings, {
    cascade: false,
    eager: false,
    nullable: false,
  })
  tool: Tool;

  @Expose()
  bookingDayWorkingHours?: WorkingHours[];
}
