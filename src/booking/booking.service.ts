import { Injectable, NotFoundException } from '@nestjs/common';
import { FindManyOptions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import moment from 'moment';
import { Booking } from './booking.entity';
import { Tool } from '../tool/entity/tool.entity';
import { User } from '../user/entity/user.entity';
import { Space } from '../organization/entity/space.entity';
import { BookingDTO } from './dto/BookingDTO';
import { UnableToBookException } from './exceptions/unable.to.book.exception';
import { SpaceNotFoundException } from '../organization/exceptions/space.not.found.exception';
import { isTimeslotAvailableInSpace } from '../domain/validators/timeslot.validator';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Injectable()
export class BookingService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingRepo: Repository<Booking>,
    @InjectRepository(Tool)
    private readonly toolsRepo: Repository<Tool>,
    @InjectRepository(User)
    private readonly usersRepo: Repository<User>,
    @InjectRepository(Space)
    private readonly spaceRepo: Repository<Space>,
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
  ) {}
  findAll(options: FindManyOptions) {
    return this.bookingRepo.find(options).then(o => {
      return o.map(item => {
        let bookingDayWorkingHours = [];
        if (
          item &&
          item.tool &&
          item.tool.controller &&
          item.tool.controller.space &&
          item.tool.controller.space.workingHours
        ) {
          const bookingDayNumber = moment(item.bookingDate).day();
          bookingDayWorkingHours = item.tool.controller.space.workingHours.filter(
            workingDay => {
              return (
                bookingDayNumber === workingDay.day && workingDay.isWorking
              );
            },
          );
        }
        item.bookingDayWorkingHours = bookingDayWorkingHours;
        return item;
      });
    });
  }

  async create(bookingDTO: BookingDTO): Promise<Booking> {
    try {
      const { timeSlot } = bookingDTO;
      const ownerOfBooking = await this.usersRepo.findOneOrFail(
        bookingDTO.ownerId,
        { relations: ['toolTemplates'] },
      );
      const buddy =
        bookingDTO.buddyEmail &&
        (await this.usersRepo.findOne({ email: bookingDTO.buddyEmail }));
      const toolForBooking = await this.toolsRepo.findOneOrFail(
        bookingDTO.toolId,
        {
          relations: ['controller'],
        },
      );
      if (!toolForBooking.controller) {
        throw new NotFoundException(
          `Tool ${bookingDTO.toolId} has no Controller.`,
        );
      }
      const bookingDate = moment(bookingDTO.bookingDate).startOf('day');
      if (!toolForBooking.controller.space.workingHours) {
        throw new NotFoundException(
          `Cannot define working hours for Space related to Tool ${
            bookingDTO.toolId
          }.`,
        );
      }
      if (
        !isTimeslotAvailableInSpace(
          await this.workingHoursRepo.find({
            where: {
              space: { id: toolForBooking.controller.space.id },
              day: bookingDate.isoWeekday(),
            },
          }),
          bookingDate.isoWeekday(),
          timeSlot,
        )
      ) {
        throw new UnableToBookException(
          'Booking is not possible: Space has no available time slot for requested time',
        );
      }

      // Also we have to check that current user has permissions on requested tool, if that tool requires training
      if (toolForBooking.template.requiresTraining) {
        const userToCheck: User[] = [ownerOfBooking];
        if (buddy) {
          userToCheck.push(buddy);
        }
        const isBookingAllowed = userToCheck.every(user =>
          user.hasToolTemplatesPermission([toolForBooking.template]),
        );
        if (!isBookingAllowed) {
          // In this case user has permissions on requested tools
          throw new UnableToBookException(
            `Booking is not possible: User has no permissions on requested tool: [${
              toolForBooking.title
            }]`,
          );
        }
      }

      // Then we should check, that selected tool isn't booked already
      const bookedTool = await this.bookingRepo
        .createQueryBuilder('booking')
        .where('time_slot @> :timeSlot', { timeSlot })
        .andWhere('date(booking_date) = :dateEvent', {
          dateEvent: bookingDTO.bookingDate,
        })
        .andWhere('"booking"."toolId" = :tool_id', {
          tool_id: bookingDTO.toolId,
        })
        .getOne();
      if (bookedTool) {
        throw new UnableToBookException(
          `Cannot book a tool: selected date and time slot already booked`,
        );
      }

      const booking = this.bookingRepo.create(bookingDTO);
      booking.tool = toolForBooking;
      booking.bookingDate = bookingDate.toDate();
      booking.owner = await this.usersRepo.findOne(bookingDTO.ownerId);
      return await this.bookingRepo.save(booking);
    } catch (e) {
      switch (e.name) {
        case 'EntityNotFound':
          throw new SpaceNotFoundException(e.message);
      }
      throw e;
    }
  }
}
