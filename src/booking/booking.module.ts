import { Module } from '@nestjs/common';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Booking } from './booking.entity';
import { Tool } from '../tool/entity/tool.entity';
import { User } from '../user/entity/user.entity';
import { Space } from '../organization/entity/space.entity';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Booking, Tool, User, Space, WorkingHours]),
  ],
  controllers: [BookingController],
  providers: [BookingService],
})
export class BookingModule {}
