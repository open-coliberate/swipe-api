import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Injectable, Logger } from '@nestjs/common';
import { TimeSlot } from '../../domain/interfaces/timeslot.interface';

export const isTimeSlot = (argument: any): argument is TimeSlot => {
  return (
    argument.hasOwnProperty('from') &&
    argument.hasOwnProperty('to') &&
    argument.from < argument.to
  );
};

@ValidatorConstraint({ name: 'IsTimeSlot' })
@Injectable()
export class IsTimeSlotConstraint implements ValidatorConstraintInterface {
  async validate(validationArguments: ValidationArguments) {
    try {
      return isTimeSlot(validationArguments);
    } catch (e) {
      Logger.error(
        `IsBookingTimeSlotConstraint has failed with error: ${e.message}`,
      );
      return false;
    }
  }
  defaultMessage(args: ValidationArguments) {
    return `Passed value isn't time slot or [from] value lower than [to]!`;
  }
}

export function IsTimeSlot(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsTimeSlotConstraint,
    });
  };
}
