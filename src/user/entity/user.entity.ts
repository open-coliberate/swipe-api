import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Exclude, Expose, Transform } from 'class-transformer';
import { Roles } from '../../auth/enums/roles.enum';
import { CoreEntity } from '../../domain/entity/core.entity';
import { AuthToken } from '../../auth/entity/auth.token.entity';
import { ResetPasswordRequest } from '../../auth/entity/reset.password.request.entity';
import { RFIDCard } from '../../rfid/entity/rfid.card.entity';
import { Organization } from '../../organization/entity/organization.entity';
import { Booking } from '../../booking/booking.entity';
import { ToolTemplate } from '../../tool/entity/tool.template.entity';
import { ApiModelProperty } from '@nestjs/swagger';
import { WorkingHours } from '../../organization/entity/working.hours.entity';

@Exclude()
@Entity('users')
export class User extends CoreEntity {
  @ApiModelProperty({ readOnly: true })
  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  firstName: string;

  @ApiModelProperty({ readOnly: true })
  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  lastName: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  password: string;

  @ApiModelProperty({ readOnly: true, type: 'string', format: 'email' })
  @Expose()
  @Column({
    type: 'varchar',
    unique: true,
  })
  email: string;

  @ApiModelProperty()
  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  phone: string;

  @ApiModelProperty({ readOnly: true })
  @Expose()
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  @Expose()
  isActive: boolean = false;

  @ApiModelProperty({ readOnly: true, type: String })
  @Expose()
  @Column({
    type: 'varchar',
    default: Roles.GENERAL_USER,
  })
  role: string;

  @ApiModelProperty({ readOnly: true })
  @OneToOne(() => AuthToken, {
    eager: true,
    cascade: true,
  })
  @JoinColumn()
  token: AuthToken;

  @OneToOne(() => ResetPasswordRequest, {
    eager: false,
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  resetPasswordRequest: ResetPasswordRequest;

  @OneToOne(() => RFIDCard, {
    cascade: true,
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  rfidCard: RFIDCard;

  @Expose()
  @OneToMany(type => WorkingHours, workingHours => workingHours.user, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  workingHours: WorkingHours[];

  @ApiModelProperty({ readOnly: true })
  @Expose()
  @Transform(organization => {
    return organization
      ? {
          id: organization.id,
          title: organization.title,
        }
      : null;
  })
  @ManyToOne(type => Organization, organization => organization.users, {
    eager: true,
    nullable: false,
    cascade: false,
    onDelete: 'SET NULL',
  })
  organization: Organization;

  @OneToMany(type => Booking, booking => booking.owner, {
    cascade: false,
    onDelete: 'SET NULL',
  })
  bookings: Booking[];

  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  photo?: string;

  @ManyToMany(type => ToolTemplate, toolTemplate => toolTemplate.users)
  @JoinTable({ name: 'users_tool_templates' })
  toolTemplates: ToolTemplate[];

  public hasRole(...expectedRoles: string[]): boolean {
    return expectedRoles.includes(this.role);
  }
  public hasToolTemplatesPermission(toolTemplates: ToolTemplate[]): boolean {
    if (this.toolTemplates) {
      const toolTemplateIds = toolTemplates.map(value => value.id);
      return this.toolTemplates.every(toolTemplate =>
        toolTemplateIds.includes(toolTemplate.id),
      );
    }
  }
}
