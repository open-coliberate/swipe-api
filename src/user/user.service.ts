import { ForbiddenException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import CsvProcessor from 'fast-csv';
import { isEmpty } from 'lodash';
import nanoid from 'nanoid/async';
import { User } from './entity/user.entity';
import { FindOneOptions, getConnection, In, Repository } from 'typeorm';
import { RfidService } from '../rfid/rfid.service';
import { MessageType } from '../mailer/constants/mailer.constants';
import { MailerService } from '../mailer/mailer.service';
import { ValidationPipe } from '../application/pipes/validation.pipe';
import { ResetPasswordRequest } from '../auth/entity/reset.password.request.entity';
import { Organization } from '../organization/entity/organization.entity';
import { ToolTemplate } from '../tool/entity/tool.template.entity';
import { UserWithRFIDCardDto } from './dto/user.with.RFIDCard.dto';
import { OrganizationNotFoundException } from '../organization/exceptions/organization.not.found.exception';
import { AccessToToolTemplateDTO } from './dto/access.to.tool.template.dto';
import { UserSearchDto } from './dto/user.search.dto';
import { PromoteToRoleDto } from './dto/promote.to.role.dto';
import { UserNotFoundException } from './exceptions/user.not.found.exception';
import { UserProfileDto } from './dto/user.profile.dto';
import { ConfigService } from '../config/config.service';
import { UserInviteDto } from './dto/user.invite.dto';
import { SpaceService } from '../organization/services/space.service';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
    @InjectRepository(ResetPasswordRequest)
    private readonly resetPasswordRequestRepo: Repository<ResetPasswordRequest>,
    @InjectRepository(Organization)
    private readonly organizationRepo: Repository<Organization>,
    @InjectRepository(ToolTemplate)
    private readonly toolTemplateRepo: Repository<ToolTemplate>,
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
    private readonly rfidService: RfidService,
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly spaceService: SpaceService,
  ) {}
  async create(
    payload: UserWithRFIDCardDto | UserInviteDto,
    sendEmail: boolean = true,
  ): Promise<User> {
    try {
      const organization = await this.organizationRepo.findOneOrFail(
        payload.organizationId,
      );
      const user = await this.userRepo.create(payload);
      user.organization = organization;
      if (payload.rfidUID) {
        user.rfidCard = await this.rfidService.createCard({
          uid: payload.rfidUID,
        });
      }
      user.resetPasswordRequest = await this.resetPasswordRequestRepo.create({
        token: await nanoid(128),
      });

      if (sendEmail) {
        await this.mailerService.addMessageForProcessing(
          this.mailerService.getMessage(user.email, MessageType.SignUp, {
            firstName: user.firstName,
            lastName: user.lastName,
            token: user.resetPasswordRequest.token,
            hostName: process.env.APP_HOST,
          }),
        );
      }

      await this.save(user);
      return this.findOneByEmail(user.email, {
        relations: ['rfidCard'],
      });
    } catch (error) {
      switch (error.name) {
        case 'EntityNotFound': {
          throw new OrganizationNotFoundException(
            `Organization ${payload.organizationId} is not found!`,
          );
        }
        default:
          throw error;
      }
    }
  }

  async allowAccessToToolTemplate(
    user: User,
    allowAccessToToolTemplateDTO: AccessToToolTemplateDTO,
  ): Promise<User> {
    user.toolTemplates = await this.toolTemplateRepo.findByIds(
      allowAccessToToolTemplateDTO.toolTemplatesIds,
    );
    return this.userRepo.save(user);
  }

  save(user: User): Promise<User> {
    return this.userRepo.save(user);
  }

  async findAll(payload: UserSearchDto): Promise<[User[], number]> {
    const alias = 'user';
    const { organizationId } = payload;
    const ITEMS_PER_PAGE = Number(this.configService.get('ITEMS_PER_PAGE'));
    const queryBuilder = this.userRepo
      .createQueryBuilder(alias)
      .limit(ITEMS_PER_PAGE)
      .offset(ITEMS_PER_PAGE * (payload.page - 1));
    if (payload.sort) {
      for (const sortItem of payload.sort) {
        const sortPayload = sortItem.split(',');
        queryBuilder.addOrderBy(
          `${alias}.${sortPayload[0]}`,
          // @ts-ignore
          sortPayload[1].toUpperCase(),
        );
      }
    }
    if (payload.search) {
      queryBuilder.where(
        `CONCAT("firstName", ' ', "lastName", ' ', "email") ILIKE '%' || :keyword || '%'`,
        { keyword: payload.search },
      );
    }
    queryBuilder.andWhere(`"${alias}"."organizationId" = :organizationId`, {
      organizationId,
    });
    queryBuilder.leftJoinAndSelect('user.organization', 'organization');
    return queryBuilder.getManyAndCount();
  }

  async findOneByEmail(
    email: string,
    options: FindOneOptions = {},
  ): Promise<User> {
    return await this.userRepo.findOne({ email }, options);
  }

  async findOneByAccessToken(token: string): Promise<User> {
    return await this.userRepo
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.token', 'token', 'token.accessToken = :token', {
        token,
      })
      .getOne();
  }

  async findOneByPasswordResetToken(token: string): Promise<User> {
    return await this.userRepo
      .createQueryBuilder('user')
      .innerJoinAndSelect(
        'user.resetPasswordRequest',
        'resetPasswordRequest',
        'resetPasswordRequest.token = :token',
        { token },
      )
      .leftJoinAndSelect('user.token', 'token')
      .getOne();
  }

  async findOneByRefreshToken(token: string): Promise<User> {
    return await this.userRepo
      .createQueryBuilder('user')
      .innerJoinAndSelect(
        'user.token',
        'token',
        'token.refreshToken = :token',
        { token },
      )
      .getOne();
  }

  async promoteToRole(
    promoteToRoleDto: PromoteToRoleDto,
    user: User,
  ): Promise<User> {
    user.role = promoteToRoleDto.role;
    return await this.save(user);
  }

  async activate(user: User) {
    user.isActive = Boolean(user.rfidCard) && Boolean(user.password);
    return await this.save(user);
  }

  async findOneById(id: string, options?: FindOneOptions<User>): Promise<User> {
    try {
      return await this.userRepo.findOneOrFail(id, options);
    } catch (e) {
      switch (e.name) {
        case 'EntityNotFound': {
          throw new UserNotFoundException(`User with ${id} is not found!`);
        }
        default:
          throw e;
      }
    }
  }

  async update(id: string, userDto: UserProfileDto): Promise<User> {
    try {
      const user = await this.findOneById(id);
      if (!isEmpty(user)) {
        await this.userRepo.update(user.id, userDto);
      }
      return user;
    } catch (e) {
      switch (e.name) {
        case 'EntityNotFound': {
          throw new UserNotFoundException(`User with ${id} is not found!`);
        }
        default:
          throw e;
      }
    }
  }

  async validateAndCreate(data: UserWithRFIDCardDto): Promise<User> {
    await new ValidationPipe().transform(data, {
      metatype: UserWithRFIDCardDto,
      type: 'custom',
    });
    return this.create(data);
  }

  async saveCsv(
    csv: string,
    defaultOrganisationId: string,
    onSuccess,
    onReject,
  ) {
    const items = [];
    const errors = [];
    await CsvProcessor.fromString(csv, { headers: true })
      .on('data', (data: UserWithRFIDCardDto) => {
        if (!data.organizationId) {
          data.organizationId = defaultOrganisationId;
        }
        items.push(
          this.validateAndCreate(data).catch(e => {
            if (e.constructor.name === 'ValidationFailedException') {
              errors.push({ email: data.email, errors: e.errors });
            } else {
              errors.push({ email: data.email, errors: e });
            }
          }),
        );
      })
      .on('end', async () => {
        Promise.all(items).then(reason => {
          onSuccess(items.length, errors);
        }, onReject);
      });
  }
}
