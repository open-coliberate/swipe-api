import { BaseException } from '../../domain/exceptions/base.exception';
import { HttpStatus } from '@nestjs/common';

export class UserNotUniqueException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status = HttpStatus.NOT_ACCEPTABLE;
  }
}
