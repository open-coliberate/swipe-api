import {
  BadRequestException,
  Body,
  Controller,
  FileInterceptor,
  ForbiddenException,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile,
  ApiOperation,
  ApiImplicitQuery,
} from '@nestjs/swagger';
import { User } from './entity/user.entity';
import { UserService } from './user.service';
import { RfidService } from '../rfid/rfid.service';
import { ToolService } from '../tool/tool.service';
import { ImageService } from '../services/image.service';
import { ConfigService } from '../config/config.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { OnlyRoles } from '../auth/decorators/roles.decorator';
import { Roles } from '../auth/enums/roles.enum';
import { UserSearchDto } from './dto/user.search.dto';
import { PromoteToRoleDto } from './dto/promote.to.role.dto';
import { RfidCardDto } from '../rfid/dto/rfid.card.dto';
import { AccessToToolTemplateDTO } from './dto/access.to.tool.template.dto';
import { StorageService } from '../services/storage.service';
import { extname } from 'path';
import { UserProfileDto } from './dto/user.profile.dto';
import { UserResponse } from './responses/user.response';
import { CurrentUser } from '../auth/decorators/current.user.decorator';
import { UserInviteDto } from './dto/user.invite.dto';
import { SpaceService } from '../organization/services/space.service';
import { WorkingHoursDto, WorkingHoursPutDto } from './dto/working.hours.dto';
import { ParamsAndBody } from '../application/decorator/custom.request.decorator';
import { HttpRequestService } from '../application/services/http.request.service';

@UseGuards(JwtAuthGuard)
@Controller('users')
@ApiUseTags('users')
export class UserController {
  constructor(
    private readonly usersService: UserService,
    private readonly rfidService: RfidService,
    private readonly toolService: ToolService,
    private readonly imageService: ImageService,
    private readonly configService: ConfigService,
    private readonly storageService: StorageService,
    private readonly spaceService: SpaceService,
    private readonly requestService: HttpRequestService,
  ) {}

  @Get('/')
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @ApiOperation({
    title:
      'Get paginated user list with filters and order (you will get users list related to you own organization)',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: [UserResponse],
  })
  @ApiBearerAuth()
  @ApiImplicitQuery({
    required: false,
    name: 'sort[]',
    isArray: true,
    type: 'string',
    collectionFormat: 'multi',
    description: 'Order settings, example: &sort=role,asc&sort=email,desc',
  })
  users(
    @Query() query: UserSearchDto,
    @CurrentUser() currentUser: User,
  ): Promise<[User[], number]> {
    query.organizationId = currentUser.organization.id;
    return this.usersService.findAll(query);
  }

  @Get(':id')
  @ApiOperation({
    title: `Get a particular user by id (If you're not a Lab manager or Super user, you will be able to get only your own user)`,
  })
  @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'User is not found',
  })
  @ApiBearerAuth()
  async getOne(
    @Param('id') id: string,
    @CurrentUser() currentUser: User,
  ): Promise<User> {
    if (currentUser.hasRole('general')) id = currentUser.id;
    return this.usersService.findOneById(id, {
      relations: ['workingHours', 'workingHours.space'],
    });
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('photo', {
      limits: {
        files: 1,
        fileSize: 1024 * 1024 * 20,
      },
      fileFilter: (req, file, callback) => {
        if (!['image/jpeg', 'image/png'].includes(file.mimetype)) {
          callback(
            new BadRequestException('Only JPG or PNG files are allowed'),
            false,
          );
        }
        callback(null, true);
      },
    }),
  )
  @ApiOperation({
    title: `Update a user profile. (If you're not a Lab manager or Super user, you will be able to update only your own user)`,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: UserResponse,
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'photo',
    required: false,
    description: 'Photo of user',
  })
  @ApiBearerAuth()
  async update(
    @Body() userDto: UserProfileDto,
    @UploadedFile() photo,
    @Param('id') id: string,
    @CurrentUser() currentUser: User,
  ): Promise<User> {
    try {
      if (currentUser.hasRole('general')) id = currentUser.id;
      const userToUpdate = await this.usersService.findOneById(id);
      let photoUrl = userToUpdate.photo;
      if (photo) {
        const s3Bucket = this.configService.get('AWS_S3_BUCKET_NAME');
        const photoFileKey = `${id}${extname(photo.originalname)}`;
        const photoBufferAfterResize = await this.imageService.resizeImage(
          photo,
        );
        const { Location } = await this.storageService.set(
          s3Bucket,
          photoFileKey,
          photoBufferAfterResize,
          photo.mimetype,
        );
        photoUrl = Location;
      }

      // we dont need passwordConfirmation field anymore.
      const { passwordConfirmation, ...validatedUserDto } = userDto;

      const updatedUser = {
        ...userToUpdate,
        ...{ photo: photoUrl },
        ...validatedUserDto,
      } as User;
      return await this.usersService.save(updatedUser as User);
    } catch (e) {
      throw new BadRequestException(
        `User ${id} cannot be updated: ${e.message}`,
      );
    }
  }

  @Post('/')
  @ApiBearerAuth()
  @OnlyRoles(Roles.SUPER_USER, Roles.OWNER)
  @ApiOperation({ title: 'Invites user' })
  async inviteUser(@Body() payload: UserInviteDto) {
    await Promise.all([
      this.usersService.create(payload),
      this.spaceService.getDefautlWithHubAndSpace(payload.organizationId),
    ]);
  }

  @Post(':id/promote')
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER)
  @ApiOperation({ title: 'Promotes user to a new role' })
  @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
  @ApiBearerAuth()
  async promoteToRole(
    @Body() promoteToRoleDto: PromoteToRoleDto,
    @Param('id') id: string,
  ) {
    const user = await this.usersService.findOneById(id);
    return await this.usersService.promoteToRole(promoteToRoleDto, user);
  }

  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER)
  @ApiOperation({ title: 'Binds a rfid card to a user' })
  @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
  @ApiBearerAuth()
  @Post(':id/bind-rfid-card')
  async bindRfidCardToUser(
    @Body() rfidCardDto: RfidCardDto,
    @Param('id') userId: string,
  ): Promise<User> {
    const user = await this.usersService.findOneById(userId);
    user.rfidCard = await this.rfidService.createCard(rfidCardDto);
    await this.usersService.save(user);
    return user;
  }

  @Post(':id/allow-tool-template')
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
  @ApiBearerAuth()
  @ApiOperation({ title: 'Allows use tool template to user' })
  async allowAccessToToolTemplate(
    @Body() allowAccessToToolTemplateDTO: AccessToToolTemplateDTO,
    @Param('id') userId: string,
  ) {
    try {
      const user = await this.usersService.findOneById(userId);
      return await this.usersService.allowAccessToToolTemplate(
        user,
        allowAccessToToolTemplateDTO,
      );
    } catch (e) {
      throw e;
    }
  }

  @ApiOperation({ title: 'Returns list of user allowed tool templates' })
  @Get(':id/trainings')
  @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
  @ApiBearerAuth()
  getTrainings(@Param('id') userId: string) {
    return this.toolService.getTrainingsByUserId(userId);
  }

  @ApiOperation({ title: 'Put working hours for user' })
  @Put('/:user/working-hours')
  @ApiBearerAuth()
  setWorkingHours(
    @ParamsAndBody({ returnClass: true }) payload: WorkingHoursPutDto,
    @CurrentUser() user,
  ) {
    if (
      user.id !== payload.user &&
      ![Roles.OWNER, Roles.SUPER_USER].includes(user.role)
    ) {
      throw new ForbiddenException('Access denied to edit this user');
    }

    payload.items = payload.items.map((o: WorkingHoursDto) => {
      o.user = payload.user;
      return o;
    });
    return this.requestService.performPut(payload);
  }
}
