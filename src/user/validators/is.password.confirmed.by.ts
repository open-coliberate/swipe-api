import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsPasswordConfirmed', async: true })
export class IsPasswordConfirmedByConstraint
  implements ValidatorConstraintInterface {
  async validate(value: string, validationArguments: ValidationArguments) {
    const [relatedPropertyName] = validationArguments.constraints;
    const relatedValue = validationArguments.object[relatedPropertyName];

    // return !user;
    return value === relatedValue;
  }
  defaultMessage(args: ValidationArguments) {
    return 'Password is not equal to password confirmation.';
  }
}

export function IsPasswordConfirmedBy(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [property],
      validator: IsPasswordConfirmedByConstraint,
    });
  };
}
