import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Inject, Injectable } from '@nestjs/common';
import { UserService } from '../user.service';

@ValidatorConstraint({ name: 'IsUserAlreadyExists', async: true })
@Injectable()
export class IsUserAlreadyExistsConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('UserService') private readonly userService: UserService,
  ) {}
  async validate(email: string, validationArguments: ValidationArguments) {
    const user = await this.userService.findOneByEmail(email);

    return !user;
  }
  defaultMessage(args: ValidationArguments) {
    return 'User with this email already exists.';
  }
}

export function IsUserAlreadyExists(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsUserAlreadyExistsConstraint,
    });
  };
}
