import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsSortPayload', async: true })
export class IsSortPayloadConstraint implements ValidatorConstraintInterface {
  async validate(
    data: Array<string>,
    validationArguments: ValidationArguments,
  ) {
    const allowedSortValues = ['asc', 'desc'];
    return data.every(item => {
      const sortPayload = item.split(',');
      return (
        validationArguments.constraints.some(
          argument => argument === sortPayload[0],
        ) && allowedSortValues.some(sortValue => sortValue === sortPayload[1])
      );
    });
  }
  defaultMessage(args: ValidationArguments) {
    return `Passed payload is not properly formed sort object: keys should be: [${args.constraints.join(
      ', ',
    )}] and values should be: [asc, desc]`;
  }
}

export function IsSortPayload(
  fieldsAllowed: Array<string>,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: fieldsAllowed,
      validator: IsSortPayloadConstraint,
    });
  };
}
