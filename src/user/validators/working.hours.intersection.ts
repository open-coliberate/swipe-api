import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Not, Repository, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { WorkingHoursPutDto } from '../dto/working.hours.dto';
import _ from 'lodash';
import { WorkingHours } from '../../organization/entity/working.hours.entity';

@ValidatorConstraint({
  name: 'WorkingHoursIntersection',
  async: true,
})
export class WorkingHoursIntersectionConstraint
  implements ValidatorConstraintInterface {
  message;
  constructor(
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
  ) {}

  defaultMessage(validationArguments?: ValidationArguments) {
    return this.message;
  }

  async validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    const payload = validationArguments.object as WorkingHoursPutDto;

    // here we are getting entities from DB that will be leave untouched after whole update operation
    const untachedEntities = payload.items.filter(o => o.id).map(o => o.id);
    const query = {
      where: {
        user: {
          id: value,
        },
      },
      relations: ['space'],
    };
    if (untachedEntities.length) {
      query.where['id'] = Not(In(untachedEntities));
    }
    const hours = await this.workingHoursRepo.find(query);

    // next step - grouping to perform validation
    const grouped = _.groupBy(
      _.sortBy([].concat(hours, payload.items.filter(o => !o.deleted)), [
        'from',
        'to',
      ]),
      'day',
    );

    // next code checks that all intervals are not overlapped
    const errors = [];
    Object.entries(grouped).forEach(day => {
      let lastElement;
      day[1].forEach(element => {
        if (lastElement && lastElement.to > element.from) {
          errors.push(
            `${this.getIdentificator(
              element,
            )} incompatible with ${this.getIdentificator(lastElement)}`,
          );
        }
        lastElement = element;
      });
    });

    this.message = errors.join('; ');
    return errors.length === 0;
  }

  private getIdentificator(element): string {
    const date = new Date(element.from);
    const exists = element.id ? ' that already exists' : '';
    const space = element.id ? element.space.id : element.space;
    return `element${exists} with from at ${date.getHours()}:${date.getMinutes()} in space [#${space}]`;
  }
}

export function WorkingHoursIntersection(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: WorkingHoursIntersectionConstraint,
    });
  };
}
