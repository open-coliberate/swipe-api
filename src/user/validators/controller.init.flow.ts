import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Inject } from '@nestjs/common';
import { UserService } from '../user.service';
import { ControllerGetInitInfoDto } from '../../auth/dto/controller.get.init.info.dto';
import { User } from '../entity/user.entity';
import { Organization } from '../../organization/entity/organization.entity';
import { OrganizationService } from '../../organization/services/organization.service';
import { Roles } from '../../auth/enums/roles.enum';

@ValidatorConstraint({
  name: 'IsUserLabManagerByOrganizationTitle',
  async: true,
})
export class IsUserLabManagerByOrganizationTitleConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('UserService') private readonly userService: UserService,
    @Inject('OrganizationService')
    private readonly organizationService: OrganizationService,
  ) {}

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `User with this email and RFID card have not access to this organization or not exists`;
  }

  async validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    const item = validationArguments.object as ControllerGetInitInfoDto;
    return await validateUserOrganizationAndCard(
      this.userService.findOneByEmail(value, { relations: ['rfidCard'] }),
      this.organizationService.findOneByTitle(item.organizationTitle),
      item.RFIDCardCode,
    );
  }
}

export async function validateUserOrganizationAndCard(
  userPromise: Promise<User> | User,
  organizationPromise: Promise<Organization> | Organization,
  cardUid: string,
): Promise<boolean> {
  const [user, organization] = await Promise.all([
    userPromise,
    organizationPromise,
  ]);

  return (
    user &&
    organization &&
    [Roles.OWNER, Roles.SUPER_USER, Roles.LAB_MANAGER].includes(
      user.role as Roles,
    ) &&
    user.isActive &&
    user.organization.id === organization.id &&
    user.rfidCard &&
    user.rfidCard.uid === cardUid
  );
}

export function IsUserLabManagerByOrganizationTitle(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsUserLabManagerByOrganizationTitleConstraint,
    });
  };
}
