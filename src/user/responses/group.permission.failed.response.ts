import { ApiModelProperty } from '@nestjs/swagger';

abstract class FieldError {
  @ApiModelProperty({
    description: 'Validation error message for field',
    type: 'string',
    example: ['Card already in use. Please choose another one!'],
  })
  fieldName: Array<string>;
}

export class GroupPermissionFailedResponse {
  @ApiModelProperty({
    description: 'Validation error message',
    type: 'string',
    example: 'Validation failed',
  })
  message: string;

  @ApiModelProperty({
    description: 'Validation errors array',
    type: FieldError,
    isArray: true,
    example: [
      {
        rfidUID: ['Card already in use. Please choose another one!'],
      },
      {
        email: ['User with this email already exists.'],
      },
    ],
  })
  errors: Array<FieldError>;
}
