import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { UserResponse } from './user.response';
import { BaseResponse } from './base.response';
abstract class AuthToken extends BaseResponse {
  @ApiModelProperty({
    description: 'Access token',
    type: 'string',
    example: `0KxjLyIDRPk/QA/L3vS8sq90FMkFzHEpBarFJ5c+VuKi3H2ISH9LV2F9/wgkbuTt`,
  })
  accessToken: string;
  @ApiModelProperty({
    description: 'Refresh token',
    type: 'string',
    example: `+LrPtZRtiyTq/B8Vhh+beQtzrppOSuP50mSrmCcIuXFR0/+rKefZ1uQV8m4elQrd`,
  })
  refreshToken: string;
}
export class UserResponseWithToken extends UserResponse {
  @ApiModelPropertyOptional({
    description: 'User profile photo',
    type: AuthToken,
  })
  token: AuthToken;
}
