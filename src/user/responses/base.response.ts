import { ApiModelProperty } from '@nestjs/swagger';

export abstract class BaseResponse {
  @ApiModelProperty({
    description: 'Id',
    type: 'string',
    format: 'uuid',
    example: 'da6a6f55-c269-4d88-81e7-45fce37e09a5',
  })
  id: string;

  @ApiModelProperty({
    description: 'Created at date',
    type: 'string',
    format: 'date',
    example: '2019-02-11T10:59:12.068Z',
  })
  createdAt: Date;

  @ApiModelProperty({
    description: 'Updated at date',
    type: 'string',
    format: 'date',
    example: '2019-02-11T10:59:12.068Z',
  })
  updatedAt: Date;
}
