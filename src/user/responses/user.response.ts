import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Roles } from '../../auth/enums/roles.enum';
import { BaseResponse } from './base.response';

abstract class OrganizationType extends BaseResponse {
  @ApiModelProperty({
    description: 'Organization name',
    type: 'string',
    example: 'Coliberate.io',
  })
  title: string;
}

export class UserResponse extends BaseResponse {
  @ApiModelPropertyOptional({
    description: 'First name',
    type: 'string',
    example: 'John',
  })
  firstName: string;

  @ApiModelPropertyOptional({
    description: 'Last name',
    type: 'string',
    example: 'Doe',
  })
  lastName: string;

  @ApiModelProperty({
    description: 'Email',
    type: 'string',
    format: 'email',
    example: 'john.doe@corp.umbrella.com',
  })
  email: string;

  @ApiModelPropertyOptional({
    description: 'Description',
    type: 'string',
    example: `Hello, my name is John, and i'm in love with possums`,
  })
  description: string;

  @ApiModelPropertyOptional({
    description: 'Current user role',
    type: 'string',
    example: 'general',
  })
  role: Roles;

  @ApiModelProperty({
    description: 'User related Organization',
    type: OrganizationType,
  })
  organization: OrganizationType;

  @ApiModelPropertyOptional({
    description: 'User profile photo',
    type: 'string',
    example:
      'https://media.gannett-cdn.com/poughkeepsie/41819768001/201703/238/41819768001_5376639983001_5376616282001-vs.jpg',
  })
  photo: string;
}
