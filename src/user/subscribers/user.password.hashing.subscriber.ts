import {
  EntitySubscriberInterface,
  InsertEvent,
  EventSubscriber,
  UpdateEvent,
} from 'typeorm';
import { hash } from 'bcrypt';

import { User } from '../entity/user.entity';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class UserPasswordHashingSubscriber
  implements EntitySubscriberInterface {
  saltRounds: number = 10;
  listenTo() {
    return User;
  }
  async beforeInsert(event: InsertEvent<User>) {
    const { entity } = event;
    if (entity.password) {
      Logger.log(`A new hashed password for ${entity.email} will be stored.`);
      entity.password = await hash(entity.password, this.saltRounds);
    }
  }
  async beforeUpdate(event: UpdateEvent<User>) {
    const { entity, updatedColumns } = event;
    if (entity) {
      const updatedColumnNames = updatedColumns.map(
        column => column.propertyName,
      );
      if (updatedColumnNames.includes('password')) {
        Logger.log(
          `Password for ${
            entity.email
          } has been changed - this action will produce a new password hash`,
        );
        entity.password = await hash(entity.password, this.saltRounds);
      } else {
        Logger.log(
          `Password for ${
            entity.email
          } has not changed - just skip a new password hash generation`,
        );
        return;
      }
    }
    return;
  }
}
