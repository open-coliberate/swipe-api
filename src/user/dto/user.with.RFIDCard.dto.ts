import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserBaseDto } from './user.base.dto';
import { IsRFIDCardAlreadyInUse } from '../../rfid/validators/is.card.already.in.use';

export class UserWithRFIDCardDto extends UserBaseDto {
  @ApiModelProperty({
    description: 'RFID Card uid',
    required: true,
  })
  @IsNotEmpty()
  @IsRFIDCardAlreadyInUse()
  rfidUID: string;

  password?: string;
}
