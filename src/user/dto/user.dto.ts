import { IsNotEmpty, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserBaseDto } from './user.base.dto';

export class UserDto extends UserBaseDto {
  @ApiModelProperty({
    description: 'Password',
    required: true,
  })
  @IsNotEmpty()
  @MinLength(6)
  password: string;
}
