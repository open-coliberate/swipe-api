import { PaginateRequestBaseDto } from '../../domain/dto/paginate.request.base.dto';

import { IsArray, IsUUID, ValidateIf } from 'class-validator';
import { IsSortPayload } from '../validators/is.sort.payload';

export class UserSearchDto extends PaginateRequestBaseDto {
  @ValidateIf(dto => dto.organizationId)
  @IsUUID()
  organizationId: string;

  @IsArray()
  @IsSortPayload(['role', 'email', 'firstName', 'isActive'])
  sort: [string];
}
