import {
  IsBoolean,
  IsOptional,
  IsString,
  IsUUID,
  Max,
  Min,
  Validate,
  ValidateIf,
} from 'class-validator';
import {
  PutRequestDto,
  RequestDeletable,
} from '../../application/dto/http.request.dto';
import { ForeignKeyExists } from '../../application/validator/foreign.key.exists.validator';
import { User } from '../entity/user.entity';
import { Space } from '../../organization/entity/space.entity';
import {
  Day,
  WorkingHours as WorkingHoursInterface,
} from '../../domain/interfaces/working.hours.interface';
import { WorkingHours } from '../../organization/entity/working.hours.entity';
import { WorkingHoursIntersection } from '../validators/working.hours.intersection';
import { GreaterThan } from '../../application/validator/numbers.validator';

export class WorkingHoursPutDto extends PutRequestDto {
  dtoType = WorkingHoursDto;

  @WorkingHoursIntersection()
  @ForeignKeyExists(User)
  user: string;
}

export class WorkingHoursDto extends RequestDeletable
  implements WorkingHoursInterface {
  entityType = WorkingHours;

  @ValidateIf(o => !o.deleted)
  @Min(0)
  @Max(6)
  day: Day;

  @ValidateIf(o => !o.deleted)
  @ForeignKeyExists(Space)
  space: string;

  @ValidateIf(o => !o.deleted)
  @Min(0)
  @Max(86399)
  from: number;

  @ValidateIf(o => !o.deleted)
  @Min(0)
  @Max(86399)
  @GreaterThan('from')
  to: number;

  user: string;
  id: string;

  @ValidateIf(o => !o.deleted)
  @IsBoolean()
  isWorking: boolean;
}
