import { IsEmpty, IsNotEmpty, MinLength, ValidateIf } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsPasswordConfirmedBy } from '../validators/is.password.confirmed.by';

export class UserProfileDto {
  @ApiModelPropertyOptional({
    description: 'First name',
  })
  readonly firstName?: string;

  @ApiModelPropertyOptional({
    description: 'Last name',
  })
  readonly lastName?: string;

  @ApiModelProperty({
    description: 'Password',
    required: true,
  })
  @ValidateIf(u => u.password)
  @IsNotEmpty()
  @IsPasswordConfirmedBy('passwordConfirmation')
  @MinLength(6)
  readonly password: string;

  @ApiModelProperty({
    description: 'Password confirmation',
    required: true,
  })
  readonly passwordConfirmation?: string;

  @ValidateIf(u => u.photo)
  @IsNotEmpty()
  readonly photo?: string;

  @ApiModelProperty({
    description: 'Description of user',
  })
  @ValidateIf(u => u.description)
  readonly description?: string;

  @IsEmpty()
  email?: string;

  @ValidateIf(u => u.phone)
  @IsNotEmpty()
  readonly phone: string;
}
