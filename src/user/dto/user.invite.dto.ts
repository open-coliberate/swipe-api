import { IsEmail, IsNotEmpty, IsUUID, ValidateIf } from 'class-validator';
import { IsUserAlreadyExists } from '../validators/is.user.already.exists';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOrganizationExist } from '../../organization/validators/is.organization.exists';

export class UserInviteDto {
  @ApiModelProperty({
    description: 'Email',
    required: true,
  })
  @IsNotEmpty()
  @IsEmail()
  @IsUserAlreadyExists()
  email: string;

  @ApiModelProperty({
    description: 'User related Organization',
    required: true,
  })
  @IsNotEmpty()
  @IsUUID()
  @IsOrganizationExist()
  organizationId: string;

  rfidUID: string;
}
