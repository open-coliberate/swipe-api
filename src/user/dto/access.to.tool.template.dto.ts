import { IsArray, IsUUID } from 'class-validator';
import { IsToolTemplateRequiresTraining } from '../../tool/validators/is.tool.template.requires.training.validator';

export class AccessToToolTemplateDTO {
  @IsArray()
  @IsUUID('4', { each: true })
  @IsToolTemplateRequiresTraining({ each: true })
  toolTemplatesIds: string[];
}
