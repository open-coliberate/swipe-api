import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UserSignInDto {
  @ApiModelProperty({
    type: 'string',
    description: 'Email',
    required: true,
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiModelProperty({
    type: 'string',
    description: 'Password',
    required: true,
  })
  @IsNotEmpty()
  password: string;
}
