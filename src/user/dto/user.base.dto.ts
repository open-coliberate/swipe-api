import { IsEmail, IsNotEmpty, IsUUID, ValidateIf } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsUserAlreadyExists } from '../validators/is.user.already.exists';
import { IsOrganizationExist } from '../../organization/validators/is.organization.exists';

export class UserBaseDto {
  @ApiModelProperty({
    description: 'First name',
    required: false,
  })
  firstName: string;

  @ApiModelProperty({
    description: 'Last name',
    required: false,
  })
  lastName: string;

  @ApiModelProperty({
    description: 'Email',
    required: true,
  })
  @IsNotEmpty()
  @IsEmail()
  @IsUserAlreadyExists()
  @ValidateIf(u => u.email)
  email: string;

  @ApiModelPropertyOptional({
    description: 'Description',
  })
  description: string;

  @ApiModelProperty({
    description: 'User related Organization',
    required: true,
  })
  @IsNotEmpty()
  @IsUUID()
  @IsOrganizationExist()
  @ValidateIf(u => u.organizationId)
  organizationId: string;

  @ValidateIf(u => u.photo)
  @IsNotEmpty()
  photo?: string;
}
