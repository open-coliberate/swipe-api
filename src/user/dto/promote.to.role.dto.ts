import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';
import { Roles } from '../../auth/enums/roles.enum';
import { IsOrganizationExist } from '../../organization/validators/is.organization.exists';

export class PromoteToRoleDto {
  @ApiModelProperty({
    description: 'Role should be present',
    required: true,
    enum: Roles,
  })
  role: string;

  @ApiModelProperty({
    description: 'Organization UUID',
    required: true,
  })
  @IsUUID()
  @IsOrganizationExist()
  organizationId: string;
}
