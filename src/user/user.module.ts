import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { RfidModule } from '../rfid/rfid.module';
import { ApplicationMailerModule } from '../mailer/mailer.module';
import { MailerService } from '../mailer/mailer.service';
import { ToolService } from '../tool/tool.service';
import { ConfigModule } from '../config/config.module';
import { User } from './entity/user.entity';
import { AuthToken } from '../auth/entity/auth.token.entity';
import { ResetPasswordRequest } from '../auth/entity/reset.password.request.entity';
import { Organization } from '../organization/entity/organization.entity';
import { Tool } from '../tool/entity/tool.entity';
import { ToolTemplate } from '../tool/entity/tool.template.entity';
import { ImageService } from '../services/image.service';
import { IsUserLabManagerByOrganizationTitleConstraint } from './validators/controller.init.flow';
import { OrganizationService } from '../organization/services/organization.service';
import { FileSystemService } from '../services/file.system.service';
import { StorageService } from '../services/storage.service';
import { SpaceService } from '../organization/services/space.service';
import { Space } from '../organization/entity/space.entity';
import { HubService } from '../organization/services/hub.service';
import { Hub } from '../organization/entity/hub.entity';
import { HttpRequestService } from '../application/services/http.request.service';
import { WorkingHoursIntersectionConstraint } from './validators/working.hours.intersection';
import { WorkingHours } from '../organization/entity/working.hours.entity';

const validators = [
  IsUserLabManagerByOrganizationTitleConstraint,
  WorkingHoursIntersectionConstraint,
];
@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      AuthToken,
      ResetPasswordRequest,
      Organization,
      Tool,
      ToolTemplate,
      Space,
      Hub,
      WorkingHours,
    ]),
    RfidModule,
    ConfigModule,
    ApplicationMailerModule,
  ],
  providers: [
    UserService,
    MailerService,
    ToolService,
    OrganizationService,
    ImageService,
    FileSystemService,
    StorageService,
    SpaceService,
    HubService,
    ConfigModule,
    HttpRequestService,
    ...validators,
  ],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
