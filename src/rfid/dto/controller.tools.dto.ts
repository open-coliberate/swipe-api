import { IsUUID } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ControllerToolsDto {
  @ApiModelProperty()
  @IsUUID('4', { each: true })
  toolIds: string[];
}
