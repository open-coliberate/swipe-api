import { Space } from '../../organization/entity/space.entity';
import { IsNotEmpty, IsString } from 'class-validator';

export class RfidControllerDto {
  @IsNotEmpty()
  @IsString()
  id: string;

  @IsNotEmpty()
  space: Space;
}
