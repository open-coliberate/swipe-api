import { IsEmail, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsSecretKeyValid } from '../validators/is.secret.key.valid';
import { IsHubInOrganizationValid } from '../../organization/validators/is.hub.in.organization.valid';
import { IsUserLabManagerByOrganizationTitle } from '../../user/validators/controller.init.flow';

export class RfidControllerInitInNewHubConfirmation {
  @ApiModelProperty({
    description: 'Id of the controller',
    required: true,
    format: 'UUID',
  })
  @IsNotEmpty()
  @IsUUID()
  @IsSecretKeyValid()
  deviceId: string;

  @ApiModelProperty({
    description: 'Secret string received in email by LM',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  secretKey: string;

  @ApiModelProperty({
    description: 'Id of hub to which controller will be bind',
    required: true,
  })
  @IsHubInOrganizationValid()
  @IsUUID()
  hubId: string;

  @ApiModelProperty({
    description: `Lab manager's email`,
    required: true,
    format: 'email',
  })
  @IsNotEmpty()
  @IsEmail()
  labManagerEmail: string;

  @ApiModelProperty({
    description: 'Card code of the lab manager',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  RFIDCardCode: string;
}
