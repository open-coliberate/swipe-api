import { IsNotEmpty } from 'class-validator';
import { IsRFIDCardAlreadyInUse } from '../validators/is.card.already.in.use';
import { ApiModelProperty } from '@nestjs/swagger';

export class RfidCardDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsRFIDCardAlreadyInUse()
  uid: string;
}
