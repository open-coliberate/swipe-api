import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { RFIDCard } from './entity/rfid.card.entity';
import { RFIDController } from './entity/rfid.controller.entity';
import { RfidCardDto } from './dto/rfid.card.dto';
import { ConfigService } from '../config/config.service';

// TODO Avoid using repos in service, separate to several service preferred
@Injectable()
export class RfidService {
  constructor(
    @InjectRepository(RFIDCard)
    private readonly rfidCardRepository: Repository<RFIDCard>,
    @InjectRepository(RFIDController)
    private readonly rfidControllerRepository: Repository<RFIDController>,
    private readonly configService: ConfigService,
  ) {}

  async findAllCards(): Promise<RFIDCard[]> {
    return await this.rfidCardRepository.find();
  }

  async createCard(rfidCardDto: RfidCardDto): Promise<RFIDCard> {
    return await this.rfidCardRepository.create(rfidCardDto);
  }

  async findOneCardOrFail(payload) {
    return await this.rfidCardRepository.findOneOrFail(payload);
  }

  async findOneCard(payload) {
    return await this.rfidCardRepository.findOne(payload);
  }

  async findOneController(payload, options: FindOneOptions = {}) {
    return await this.rfidControllerRepository.findOne(payload, options);
  }

  async findAllControllers(): Promise<RFIDController[]> {
    return await this.rfidControllerRepository.find();
  }

  async setInitKeyToController(
    controller: RFIDController,
    key: string | number | null,
  ): Promise<RFIDController> {
    controller.initKey = key === null ? null : String(key);
    controller.initKeyExpire =
      Date.now() +
      Number(this.configService.get('APP_CONTROLLER_INIT_TOKEN_EXPIRATION'));
    return this.rfidControllerRepository.save(controller);
  }

  async findOneControllerByRefreshToken(
    token: string,
  ): Promise<RFIDController> {
    return await this.rfidControllerRepository
      .createQueryBuilder('rfid_controller')
      .innerJoinAndSelect(
        'rfid_controller.token',
        'token',
        'token.refreshToken = :token',
        { token },
      )
      .getOne();
  }

  async updateControllerAccessToken(
    rfidController: RFIDController,
    token: string,
  ): Promise<RFIDController> {
    rfidController.token.accessToken = token;
    return await this.rfidControllerRepository.save(rfidController);
  }
}
