import { CoreEntity } from '../../domain/entity/core.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { Exclude, Expose, Transform } from 'class-transformer';
import { Space } from '../../organization/entity/space.entity';
import { AuthToken } from '../../auth/entity/auth.token.entity';

@Entity('rfid_controllers')
@Exclude()
export class RFIDController extends CoreEntity {
  @Expose()
  @Transform(token => {
    return token
      ? {
          accessToken: token.accessToken,
          refreshToken: token.refreshToken,
        }
      : null;
  })
  @OneToOne(() => AuthToken, {
    eager: false,
    cascade: true,
  })
  @JoinColumn()
  token: AuthToken;

  @Expose()
  @ManyToOne(type => Space, space => space.id, {
    onDelete: 'SET NULL',
    eager: true,
    cascade: true,
  })
  space: Space;

  @Column({
    type: 'varchar',
    nullable: true,
    default: null,
    unique: true,
  })
  initKey: string;

  @Column({
    type: 'bigint',
    nullable: true,
    default: null,
  })
  initKeyExpire: number;
}
