import { Column, Entity } from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { CoreEntity } from '../../domain/entity/core.entity';

@Entity('rfid_cards')
@Exclude()
export class RFIDCard extends CoreEntity {
  @Expose()
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  uid: string;
}
