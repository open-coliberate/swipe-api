import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Inject, Injectable } from '@nestjs/common';
import { RfidService } from '../rfid.service';

@ValidatorConstraint({ name: 'IsUserAlreadyExist', async: true })
@Injectable()
export class IsRFIDCardAlreadyInUseConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('RfidService') private readonly rfidService: RfidService,
  ) {}
  async validate(uid: string, validationArguments: ValidationArguments) {
    const card = await this.rfidService.findOneCard({ uid });

    return !card;
  }
  defaultMessage(args: ValidationArguments) {
    return 'Card already in use. Please choose another one!';
  }
}

export function IsRFIDCardAlreadyInUse(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsRFIDCardAlreadyInUseConstraint,
    });
  };
}
