import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Inject, Injectable } from '@nestjs/common';
import { RfidService } from '../rfid.service';
import { RfidControllerInitInNewHubConfirmation } from '../dto/rfid.controller.init.in.new.hub.confirmation';

@ValidatorConstraint({ name: 'IsSecretKeyValid', async: true })
@Injectable()
export class IsSecretKeyValidConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('RfidService') private readonly rfidService: RfidService,
  ) {}
  async validate(id: string, validationArguments: ValidationArguments) {
    const controller = await this.rfidService.findOneController(id),
      request = validationArguments.object as RfidControllerInitInNewHubConfirmation;
    return (
      controller &&
      controller.initKey === request.secretKey &&
      controller.initKeyExpire > Date.now()
    );
  }
  defaultMessage(args: ValidationArguments) {
    return 'Invalid pair - secretKey / id of controller or secure code is expired';
  }
}

export function IsSecretKeyValid(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsSecretKeyValidConstraint,
    });
  };
}
