import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitBody,
  ApiOperation,
  ApiResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import { RFIDCard } from './entity/rfid.card.entity';
import { RfidService } from './rfid.service';
import { ControllerToolsDto } from './dto/controller.tools.dto';
import { CurrentUser } from '../auth/decorators/current.user.decorator';
import { ToolService } from '../tool/tool.service';
import { RFIDController } from './entity/rfid.controller.entity';
import { JwtControllerGuard } from '../auth/guards/jwt-controller.guard';
import { OnlyRoles } from '../auth/decorators/roles.decorator';
import { BaseResponse } from '../domain/responses/base.response';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { Roles } from '../auth/enums/roles.enum';
import { RfidCardDto } from './dto/rfid.card.dto';

@Controller('rfid')
@ApiUseTags('rfid')
export class RfidController {
  constructor(
    private readonly rfidService: RfidService,
    private readonly toolService: ToolService,
  ) {}

  @UseGuards(JwtControllerGuard)
  @ApiOperation({
    title:
      'This action can be used to test if controller has valid access token',
  })
  @ApiBearerAuth()
  @Post('/controller/ping')
  controllerPing() {
    return 'pong';
  }

  @UseGuards(JwtControllerGuard)
  @ApiBearerAuth()
  @Patch('/controller/bind-tools')
  @ApiResponse({ status: HttpStatus.CREATED })
  @ApiOperation({
    title:
      'This action can be used to test if controller has valid access token',
  })
  async setDevices(
    @Body() payload: ControllerToolsDto,
    @CurrentUser() controller: RFIDController,
  ) {
    await this.toolService.bindToolsToController(payload.toolIds, controller);
  }

  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @ApiResponse({ status: HttpStatus.OK, type: [RfidCardDto] })
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    title: 'Returns all RFID cards',
  })
  @ApiBearerAuth()
  @Get('/cards')
  getAll(): Promise<RFIDCard[]> {
    return this.rfidService.findAllCards();
  }
}
