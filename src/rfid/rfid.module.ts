import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RfidService } from './rfid.service';
import { RfidController } from './rfid.controller';
import { ToolService } from '../tool/tool.service';
import { MailerService } from '../mailer/mailer.service';
import { UserService } from '../user/user.service';
import { RFIDCard } from './entity/rfid.card.entity';
import { RFIDController } from './entity/rfid.controller.entity';
import { Tool } from '../tool/entity/tool.entity';
import { ToolTemplate } from '../tool/entity/tool.template.entity';
import { Organization } from '../organization/entity/organization.entity';
import { User } from '../user/entity/user.entity';
import { ResetPasswordRequest } from '../auth/entity/reset.password.request.entity';
import { AuthToken } from '../auth/entity/auth.token.entity';
import { OrganizationService } from '../organization/services/organization.service';
import { IsSecretKeyValidConstraint } from './validators/is.secret.key.valid';
import { IsHubInOrganizationValidConstraint } from '../organization/validators/is.hub.in.organization.valid';
import { HubService } from '../organization/services/hub.service';
import { Hub } from '../organization/entity/hub.entity';
import { SpaceService } from '../organization/services/space.service';
import { OrganizationModule } from '../organization/organization.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RFIDCard,
      RFIDController,
      Tool,
      ToolTemplate,
      Organization,
      User,
      Hub,
      AuthToken,
      ResetPasswordRequest,
    ]),
    OrganizationModule,
  ],
  providers: [
    RfidService,
    ToolService,
    OrganizationService,
    MailerService,
    HubService,
    UserService,
    SpaceService,
    IsHubInOrganizationValidConstraint,
    IsSecretKeyValidConstraint,
  ],
  controllers: [RfidController],
  exports: [RfidService],
})
export class RfidModule {}
