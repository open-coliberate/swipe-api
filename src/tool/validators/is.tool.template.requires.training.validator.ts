import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Inject, Injectable } from '@nestjs/common';
import { ToolService } from '../tool.service';

@ValidatorConstraint({ name: 'IsToolTemplateRequiresTraining', async: true })
@Injectable()
export class IsToolTemplateRequiresTrainingConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('ToolService') private readonly toolService: ToolService,
  ) {}
  async validate(ids: string[], validationArguments: ValidationArguments) {
    const toolTemplates = await this.toolService.getTemplates(ids);
    if (toolTemplates) {
      return toolTemplates.every(item => {
        return !!item.requiresTraining;
      });
    }
  }
  defaultMessage(args: ValidationArguments) {
    return 'Training should be required for all tool templates';
  }
}
export function IsToolTemplateRequiresTraining(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsToolTemplateRequiresTrainingConstraint,
    });
  };
}
