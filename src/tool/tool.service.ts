import { Injectable } from '@nestjs/common';
import { In, Not, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ToolTemplate } from './entity/tool.template.entity';
import { Tool } from './entity/tool.entity';
import { RFIDController } from '../rfid/entity/rfid.controller.entity';
import { OrganizationService } from '../organization/services/organization.service';
import { ToolTemplateDto } from './dto/tool.template.dto';
import { ToolDto } from './dto/tool.dto';
import { ConfigService } from '../config/config.service';

@Injectable()
export class ToolService {
  constructor(
    @InjectRepository(ToolTemplate)
    private readonly toolTemplateRepo: Repository<ToolTemplate>,
    @InjectRepository(Tool)
    private readonly toolRepository: Repository<Tool>,
    private readonly configService: ConfigService,
    private readonly organizationService: OrganizationService,
  ) {}

  async createTemplate(payload: ToolTemplateDto): Promise<ToolTemplate> {
    const relatedTools = await this.toolRepository.findByIds(payload.tools);
    // We dont need a `tools` field anymore, just skip it with `rest` operator
    const { tools, ...validatedToolTemplateDto } = payload;
    const toolTemplate = this.toolTemplateRepo.create(validatedToolTemplateDto);
    toolTemplate.tools = relatedTools;
    toolTemplate.organization = await this.organizationService.findOneById(
      payload.organizationId,
    );
    return await this.toolTemplateRepo.save(toolTemplate);
  }

  async updateTemplate(templateId: string, payload: ToolTemplateDto) {
    const relatedTools = await this.toolRepository.findByIds(payload.tools);
    // We dont need a `tools` field anymore, just skip it with `rest` operator
    const { tools, ...validatedToolTemplateDto } = payload;
    await this.toolTemplateRepo.update(templateId, validatedToolTemplateDto);
    const updatedTemplate = await this.toolTemplateRepo.findOneOrFail(
      templateId,
    );
    updatedTemplate.tools = relatedTools;
    return await this.toolRepository.save(updatedTemplate);
  }

  async bindToolsToTemplate(
    tools: string[],
    templateId: string,
  ): Promise<ToolTemplate> {
    const toolsEntities = await this.toolRepository.findByIds(tools);
    const template = await this.toolTemplateRepo.findOneOrFail(templateId);
    template.tools = toolsEntities;
    return this.toolTemplateRepo.save(template);
  }

  async bindToolsToController(tools: string[], controllerId: RFIDController) {
    const existedTools = (await this.toolRepository.find({
      where: { id: In(tools) },
      select: ['id'],
    })).map(o => o.id);

    const newTools = [];
    tools.forEach(id => {
      if (!existedTools.includes(id)) {
        newTools.push(
          this.toolRepository.create({
            controller: controllerId,
            title: `Default tool ${Date.now()}`,
            id,
            bookings: [],
          }),
        );
      }
    });

    const promises = [];
    if (newTools) {
      promises.push(this.toolRepository.save(newTools));
    }
    if (existedTools) {
      promises.push(
        this.toolRepository.update(
          { id: In(existedTools) },
          { controller: controllerId },
        ),
      );
    }

    return await Promise.all(promises);
  }

  getTool(id: string): Promise<Tool> {
    return this.toolRepository.findOne(id, { relations: ['template'] });
  }

  paginateTools(page: number = 1): Promise<Tool[]> {
    const items = Number(this.configService.get('ITEMS_PER_PAGE'));
    return this.toolRepository.find({
      take: items,
      skip: (page - 1) * items,
      relations: ['template'],
      loadEagerRelations: true,
    });
  }

  createTools(payload: ToolDto[]): Promise<Tool[]> {
    return this.toolRepository.save(this.toolRepository.create(payload));
  }

  async deleteTemplate(templateId: string): Promise<void> {
    await this.toolTemplateRepo.delete(templateId);
  }

  async updateTool(toolId: string, payload: ToolDto) {
    await this.toolRepository.update(toolId, payload);
    return await this.toolRepository.findOneOrFail(toolId);
  }

  async deleteTool(toolId: string): Promise<void> {
    await this.toolRepository.delete(toolId);
  }

  getTemplate(id: string): Promise<ToolTemplate> {
    return this.toolTemplateRepo.findOne(id, { relations: ['tools'] });
  }

  async getTrainingsByUserId(userId: string): Promise<any> {
    const allowed = await this.toolTemplateRepo
      .createQueryBuilder('toolTemplate')
      .innerJoin('toolTemplate.users', 'users', 'users.id = :userId', {
        userId,
      })
      .where('toolTemplate.requiresTraining = true')
      .innerJoinAndSelect('toolTemplate.organization', 'organization')
      .getMany();

    const allowedTemplatesIDS = allowed.map(item => item.id);

    let notAllowed = [];
    if (allowedTemplatesIDS.length > 0) {
      notAllowed = await this.toolTemplateRepo.find({
        id: Not(In(allowedTemplatesIDS)),
      });
    }

    return {
      allowed,
      notAllowed,
    };
  }

  getAllTemplates(): Promise<ToolTemplate[]> {
    return this.toolTemplateRepo.find({ relations: ['tools'] });
  }

  getTemplates(ids: string[]): Promise<ToolTemplate[]> {
    return this.toolTemplateRepo.findByIds(ids, { relations: ['tools'] });
  }

  async paginateTemplates(page: number = 1): Promise<ToolTemplate[]> {
    const items = Number(this.configService.get('ITEMS_PER_PAGE'));
    return await this.toolTemplateRepo.find({
      relations: ['tools'],
      loadRelationIds: true,
      take: items,
      skip: (page - 1) * items,
    });
  }
}
