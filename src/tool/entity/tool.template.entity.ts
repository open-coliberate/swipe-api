import { Column, Entity, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { CoreEntity } from '../../domain/entity/core.entity';
import { Tool } from './tool.entity';
import { Organization } from '../../organization/entity/organization.entity';
import { User } from '../../user/entity/user.entity';

@Exclude()
@Entity('tool_templates')
export class ToolTemplate extends CoreEntity {
  @Expose()
  @Column('text')
  title: string;

  @Expose()
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Expose()
  @Column({
    type: 'int',
    nullable: true,
  })
  minWorkTime: number;

  @Expose()
  @Column({
    type: 'int',
    nullable: true,
  })
  maxWorkTime: number;

  @Expose()
  @Column('boolean')
  buddyMode: boolean;

  @Expose()
  @Column('boolean')
  availableInWorkingHours: boolean;

  @Expose()
  @Column('boolean')
  requiresTraining: boolean;

  @Expose()
  @Column('boolean')
  sound: boolean;

  @Expose()
  @OneToMany(type => Tool, tool => tool.template, {
    cascade: false,
  })
  tools: Tool[];

  @Expose()
  @ManyToOne(type => Organization, organization => organization.toolTemplates, {
    eager: true,
    nullable: false,
    cascade: false,
  })
  organization: Organization;

  @ManyToMany(type => User, user => user.toolTemplates)
  users: User[];
}
