import { CoreEntity } from '../../domain/entity/core.entity';
import { Column, Entity, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { Expose } from 'class-transformer';
import { ToolTemplate } from './tool.template.entity';
import { Booking } from '../../booking/booking.entity';
import { Event } from '../../event/event.entity';
import { RFIDController } from '../../rfid/entity/rfid.controller.entity';

@Entity('tools')
export class Tool extends CoreEntity {
  @Expose()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  title: string;

  @Expose()
  @ManyToOne(type => ToolTemplate, toolTemplate => toolTemplate.tools, {
    onDelete: 'SET NULL',
    eager: true,
    cascade: true,
  })
  template: ToolTemplate;

  @ManyToMany(type => Event, event => event.tools)
  events: Event[];

  @Expose()
  @ManyToOne(type => RFIDController, toolTemplate => toolTemplate.id, {
    onDelete: 'SET NULL',
    cascade: false,
    eager: true,
  })
  controller: RFIDController;

  @OneToMany(type => Booking, booking => booking.tool, {
    cascade: false,
  })
  bookings: Booking[];
}
