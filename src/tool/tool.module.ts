import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ToolService } from './tool.service';
import { ToolController } from './tool.controller';
import { ToolTemplate } from './entity/tool.template.entity';
import { Tool } from './entity/tool.entity';
import { Space } from '../organization/entity/space.entity';
import { Organization } from '../organization/entity/organization.entity';
import { OrganizationService } from '../organization/services/organization.service';
import { ConfigService } from '../config/config.service';
import { FileSystemService } from '../services/file.system.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ToolTemplate, Tool, Space, Organization]),
  ],
  providers: [
    ToolService,
    OrganizationService,
    ConfigService,
    FileSystemService,
  ],
  controllers: [ToolController],
  exports: [ToolService],
})
export class ToolModule {}
