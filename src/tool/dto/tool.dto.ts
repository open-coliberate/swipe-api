import { ApiModelProperty } from '@nestjs/swagger';
import { RFIDController } from '../../rfid/entity/rfid.controller.entity';
import { ToolTemplate } from '../entity/tool.template.entity';

export class ToolDto {
  id: string;

  @ApiModelProperty({
    description: 'Title of tool item',
    required: true,
  })
  title: string;

  @ApiModelProperty({
    description: 'Template which tool linked',
    required: false,
  })
  templateId?: string;

  template?: ToolTemplate;

  controller?: RFIDController;
}
