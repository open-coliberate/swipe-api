import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
import { IsOrganizationExist } from '../../organization/validators/is.organization.exists';

export class ToolTemplateDto {
  @ApiModelProperty({
    description: 'Title of the template',
    required: true,
  })
  title: string;

  @ApiModelProperty({
    description: 'Description of the tool template',
    required: false,
  })
  description: string;

  @ApiModelProperty({
    description: 'Minimum time of work',
    required: false,
    default: null,
  })
  minWorkTime: number;

  @ApiModelProperty({
    description: 'Maximum time of work',
    required: false,
    default: null,
  })
  maxWorkTime: number;

  @ApiModelProperty({
    description: '',
    required: false,
    default: false,
  })
  buddyMode: boolean;

  @ApiModelProperty({
    description: 'Availability in working hours',
    required: false,
    default: true,
  })
  availableInWorkingHours: boolean;

  @ApiModelProperty({
    description: 'Does tool require special training before using',
    required: false,
    default: false,
  })
  requiresTraining: boolean;

  @ApiModelProperty({
    description: 'Sound',
    required: false,
    default: false,
  })
  sound: boolean;

  @ApiModelProperty({
    description: 'Tool template Organization',
    required: true,
  })
  @IsNotEmpty()
  @IsUUID()
  @IsOrganizationExist()
  organizationId: string;

  @ApiModelProperty({
    description: 'Tool ids',
    required: false,
    type: [String],
  })
  @IsUUID('4', { each: true })
  tools: string[];
}
