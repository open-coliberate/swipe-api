import {
  ApiBearerAuth,
  ApiImplicitBody,
  ApiImplicitQuery,
  ApiOperation,
  ApiResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ToolService } from './tool.service';
import { BaseResponse } from '../domain/responses/base.response';
import { ToolTemplateDto } from './dto/tool.template.dto';
import { ToolDto } from './dto/tool.dto';
import { PaginatePageBaseDto } from '../domain/dto/paginate.request.base.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

@Controller('tools')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiUseTags('tools')
export class ToolController {
  constructor(private readonly toolService: ToolService) {}

  @Get('/templates')
  @ApiResponse({
    status: HttpStatus.OK,
    type: [ToolTemplateDto],
  })
  @ApiOperation({
    title: 'A list of tool templates',
  })
  templates() {
    return this.toolService.getAllTemplates();
  }

  @Get('/templates/:id')
  @ApiOperation({
    title: 'Get tool template by id',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: ToolTemplateDto,
  })
  template(@Param('id') id: string) {
    return this.toolService.getTemplate(id);
  }

  @Post('/template')
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BaseResponse })
  @ApiResponse({ status: HttpStatus.OK, type: ToolTemplateDto })
  @ApiOperation({
    title: 'Creating new tool template',
  })
  async createTemplate(@Body() toolTemplate: ToolTemplateDto) {
    return await this.toolService.createTemplate(toolTemplate);
  }

  @Patch('/template/:id')
  @ApiOperation({
    title: 'Tool template editing',
  })
  @ApiResponse({ status: HttpStatus.OK, type: ToolTemplateDto })
  async editTemplate(
    @Param('id') id: string,
    @Body() toolTemplate: ToolTemplateDto,
  ) {
    return await this.toolService.updateTemplate(id, toolTemplate);
  }

  @Delete('/template/:id')
  @ApiOperation({
    title: 'Tool template deleting',
  })
  @ApiResponse({ status: HttpStatus.NOT_FOUND })
  @ApiResponse({ status: HttpStatus.OK, type: ToolTemplateDto })
  async deleteTemplate(@Param('id') id: string) {
    try {
      await this.toolService.deleteTemplate(id);
    } catch (e) {
      throw new NotFoundException(`Template with ${id} is not found!`);
    }
  }

  @Get('/')
  @ApiOperation({
    title: 'Tools paginating',
  })
  @ApiResponse({ status: HttpStatus.NOT_FOUND, type: BaseResponse })
  @ApiResponse({ status: HttpStatus.OK, type: [ToolDto] })
  async tools(@Query() query: PaginatePageBaseDto) {
    return await this.toolService.paginateTools(query.page);
  }

  @Get('/:id')
  @ApiOperation({
    title: 'Get tool by id',
  })
  @ApiImplicitQuery({
    name: 'id',
    type: 'string',
    description: 'Tool id',
  })
  @ApiResponse({ status: HttpStatus.OK, type: ToolDto })
  async tool(@Param('id') id: string) {
    return await this.toolService.getTool(id);
  }

  @Post('/')
  @ApiOperation({
    title: 'Tool creating',
  })
  @ApiImplicitBody({
    name: 'Array',
    type: [ToolDto],
  })
  @ApiResponse({ status: HttpStatus.CREATED, type: ToolDto })
  async createTools(@Body() tools: ToolDto[]) {
    return await this.toolService.createTools(tools);
  }

  @Delete('/:id')
  @ApiOperation({
    title: 'Tool deleting',
  })
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  async deleteTool(@Param('id') id: string) {
    try {
      await this.toolService.deleteTool(id);
    } catch (e) {
      throw new NotFoundException(`Tool with ${id} is not found!`);
    }
  }

  @Patch('/:id')
  @ApiOperation({
    title: 'Tool edition',
  })
  @ApiImplicitQuery({
    name: 'id',
    type: 'string',
    required: true,
    description: 'Id of tool which will be edited',
  })
  @ApiImplicitBody({
    name: 'ToolDto',
    description: 'Tool body',
    type: ToolDto,
  })
  @ApiResponse({ status: HttpStatus.OK, type: ToolDto })
  async editTool(@Param('id') id: string, @Body() tool: ToolDto) {
    return await this.toolService.updateTool(id, tool);
  }

  @Post('/:id/bind-template/:templateId')
  @ApiOperation({
    title: 'Binding tool to template',
  })
  @ApiResponse({ status: HttpStatus.OK })
  @ApiImplicitQuery({
    name: 'id',
    type: 'string',
    required: true,
    description: 'Id of tool which will be bind to template',
  })
  bindTool(@Param('id') id: string, @Param('templateId') templateId: string) {
    this.toolService.bindToolsToTemplate([id], templateId);
  }

  @Delete('/:id/unbind-template')
  @ApiOperation({
    title: 'Unbinding tool from template',
  })
  @ApiImplicitQuery({
    name: 'id',
    type: 'string',
    required: true,
    description: 'Id of tool which will be unbind',
  })
  @ApiResponse({ status: HttpStatus.OK })
  unbindTool(@Param('id') id: string) {
    this.toolService.bindToolsToTemplate([id], null);
  }
}
