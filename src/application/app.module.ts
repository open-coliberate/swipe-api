import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '../config/config.module';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { RolesGuard } from '../auth/guards/roles.guard';
import { ApplicationMailerModule } from '../mailer/mailer.module';
import { RfidModule } from '../rfid/rfid.module';
import { OrganizationModule } from '../organization/organization.module';
import { ToolModule } from '../tool/tool.module';
import { IsRFIDCardAlreadyInUseConstraint } from '../rfid/validators/is.card.already.in.use';
import { IsOrganizationExistConstraint } from '../organization/validators/is.organization.exists';
import { BookingModule } from '../booking/booking.module';
import { MailerService } from '../mailer/mailer.service';
import { ApplicationErrorFilter } from './app.error.filter';
import { EventModule } from '../event/event.module';
import { IsSpaceExistConstraint } from '../organization/validators/is.space.exists';
import { CommandModule } from 'nestjs-command';
import { CliModule } from '../cli/cli.module';
import { IsUserAlreadyExistsConstraint } from '../user/validators/is.user.already.exists';
import { IsTimeSlotConstraint } from '../booking/validators/is.timeslot.validator';
import { IsToolTemplateRequiresTrainingConstraint } from '../tool/validators/is.tool.template.requires.training.validator';
import { OrganizationService } from '../organization/services/organization.service';
import { SpaceService } from '../organization/services/space.service';
import { HubService } from '../organization/services/hub.service';
import { ValidationPipe } from './pipes/validation.pipe';
import { ForeignKeyExistsValidator } from './validator/foreign.key.exists.validator';
import { HttpRequestService } from './services/http.request.service';
import { GreaterThanConstraint } from './validator/numbers.validator';

const validationConstraints = [
  IsUserAlreadyExistsConstraint,
  IsRFIDCardAlreadyInUseConstraint,
  IsOrganizationExistConstraint,
  IsTimeSlotConstraint,
  IsToolTemplateRequiresTrainingConstraint,
  IsSpaceExistConstraint,
  ForeignKeyExistsValidator,
  GreaterThanConstraint,
];
@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRoot(),
    UserModule,
    AuthModule,
    RfidModule,
    ToolModule,
    ApplicationMailerModule,
    OrganizationModule,
    BookingModule,
    EventModule,
    CommandModule,
    CliModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    UserService,
    OrganizationService,
    HttpRequestService,
    MailerService,
    SpaceService,
    HubService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: ApplicationErrorFilter,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    ...validationConstraints,
  ],
})
export class AppModule {}
