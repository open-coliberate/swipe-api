import {
  IsBoolean,
  IsNotEmpty,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ForeignKeyExists } from '../validator/foreign.key.exists.validator';
import { CoreEntity } from '../../domain/entity/core.entity';

export abstract class RequestDeletable {
  abstract entityType: new ({}) => CoreEntity;

  @ValidateIf(o => o.deleted)
  @ForeignKeyExists('entityType')
  id?: string;

  @IsBoolean()
  @IsNotEmpty()
  deleted: boolean;
}

export abstract class PutRequestDto {
  abstract dtoType: new ({}) => RequestDeletable;

  @ValidateNested({
    each: true,
    always: true,
  })
  @Type(o => o.newObject.dtoType)
  @IsNotEmpty()
  items: RequestDeletable[];
}
