import { ApiModelProperty } from '@nestjs/swagger';

export class PingPongResponseDto {
  @ApiModelProperty({
    type: 'string',
    default: 'pong',
    example: 'pong',
  })
  ping: string;
}
