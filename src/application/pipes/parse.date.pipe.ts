import {
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { isDate } from 'lodash';

export class ParseDatePipe implements PipeTransform<string, Date> {
  transform(value: string, metadata: ArgumentMetadata): Date {
    const val = new Date(value);
    if (!isDate(val)) {
      throw new BadRequestException(
        'Transformation string top date has been failed',
      );
    }
    return val;
  }
}
