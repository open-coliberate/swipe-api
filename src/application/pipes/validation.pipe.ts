import { PipeTransform, Injectable, HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { ValidationFailedException } from '../../domain/exceptions/validation.failed.exception';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, payload) {
    const { metatype, data } = payload;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new ValidationFailedException(
        'Validation failed',
        HttpStatus.BAD_REQUEST,
        this.formatErrors(errors),
      );
    }

    return data && data.returnClass ? object : value;
  }

  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find(type => metatype === type);
  }
  private formatErrors(errors: any[], path: string = '$') {
    const pathPartial = path;
    return errors.map(error => {
      if (!error.constraints) {
        return {
          [error.property]: this.formatErrors(
            error.children,
            `${pathPartial}.${error.property}`,
          ),
        };
      }
      return {
        [error.property]: Object.values(error.constraints),
      };
    });
  }
}
