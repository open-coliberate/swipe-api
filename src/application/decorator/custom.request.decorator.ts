import { createParamDecorator } from '@nestjs/common';

export const ParamsAndBody = createParamDecorator((data, req) => {
  return Object.assign({}, req.body, req.params);
});

export const QueryAndParams = createParamDecorator((data, req) => {
  return Object.assign({}, req.query, req.params);
});
