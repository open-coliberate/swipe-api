import { Get, Controller, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { PingPongResponseDto } from './dto/ping.pong.response.dto';

@Controller()
@ApiUseTags('application')
@ApiResponse({ status: HttpStatus.OK })
export class AppController {
  @Get('/ping')
  @ApiOperation({
    title: 'Application status checking',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'API is operational',
    type: PingPongResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  ping() {
    return { ping: 'pong' };
  }
}
