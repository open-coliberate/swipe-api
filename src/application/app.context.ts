import helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';

let context = null;
export const ApplicationContext = async () => {
  if (!context) {
    const isDevelopment = process.env.NODE_ENV === 'development';
    context = await NestFactory.create(AppModule, {
      cors: {
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
        credentials: true,
      },
    });
    useContainer(context.select(AppModule), { fallbackOnErrors: true });
    const configService = context.get('ConfigService');
    const basePath = configService.get('APP_BASE_API_PATH');
    context.setGlobalPrefix(basePath);
    if (!isDevelopment) {
      context.use(helmet());
    }
    const swaggerOptions = new DocumentBuilder()
      .setTitle('Coliberate API')
      .setDescription('This page provides Coliberate API v1 documentation')
      .setVersion('0.2')
      .setBasePath(`${basePath}`)
      .addBearerAuth('Authorization', 'header')
      .setSchemes(isDevelopment ? 'http' : 'https')
      .build();
    const swaggerDocument = SwaggerModule.createDocument(
      context,
      swaggerOptions,
    );

    SwaggerModule.setup(`${basePath}/docs`, context, swaggerDocument);
  }
  return context;
};
