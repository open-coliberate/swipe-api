import { PutRequestDto, RequestDeletable } from '../dto/http.request.dto';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

export class HttpRequestService {
  constructor(@InjectConnection() private connection: Connection) {}

  performPut(payload: PutRequestDto) {
    const promises = [];
    payload.items.map((item: RequestDeletable) => {
      const { id, deleted, entityType, ...partial } = item;
      const repo = this.connection.getRepository(entityType);
      let operation: Promise<any>;
      if (deleted) {
        operation = repo.delete(id);
      } else if (item.id === null) {
        operation = repo.save(repo.create(partial));
      } else {
        operation = repo.update(id, partial);
      }
      promises.push(operation);
    });
    return Promise.all(promises);
  }
}
