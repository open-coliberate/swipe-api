import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({
  name: 'GreaterThan',
})
export class GreaterThanConstraint implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Value should be greater than '${
      validationArguments.constraints[0]
    }'`;
  }

  validate(value: any, validationArguments?: ValidationArguments): boolean {
    const payload: object = validationArguments.object;
    return value > payload[validationArguments.constraints[0]];
  }
}

export function GreaterThan(
  key: string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [key],
      validator: GreaterThanConstraint,
    });
  };
}
