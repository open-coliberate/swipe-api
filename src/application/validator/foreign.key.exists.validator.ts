import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';
import { CoreEntity } from '../../domain/entity/core.entity';

@ValidatorConstraint({
  async: true,
})
export class ForeignKeyExistsValidator implements ValidatorConstraintInterface {
  constructor(@InjectConnection() private connection: Connection) {}

  defaultMessage(validationArguments?: ValidationArguments): string {
    return 'Foreign entity not found';
  }

  validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> | boolean {
    let pointer = validationArguments.constraints[0];
    if (typeof pointer === 'string') {
      pointer = validationArguments.object[pointer];
    }
    const repo = this.connection.getRepository(pointer);
    return repo
      .count({
        where: {
          id: value,
        },
      })
      .then(item => Boolean(item));
  }
}

export function ForeignKeyExists(
  property: (new ({}) => CoreEntity) | string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'ForeignKeyExists',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: ForeignKeyExistsValidator,
    });
  };
}
