import { ApiModelProperty } from '@nestjs/swagger';
import { IsArray, IsString, Min, ValidateIf } from 'class-validator';
import { Transform } from 'class-transformer';

export class PaginatePageBaseDto {
  @ApiModelProperty({
    description: 'List page number',
    required: true,
  })
  @Transform(Number)
  @Min(1)
  page: number;
}

export class PaginateRequestBaseDto extends PaginatePageBaseDto {
  @ApiModelProperty({
    description:
      'Search string (participates in LIKE query for firstName, LastName, email fields)',
    required: false,
  })
  @ValidateIf(o => o.search)
  @IsString()
  search: string;

  @ValidateIf(o => o.sort)
  @IsArray()
  sort: [string];
}
