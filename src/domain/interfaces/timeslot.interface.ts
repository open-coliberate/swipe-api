export interface TimeSlot {
  from: number;
  to: number;
}
