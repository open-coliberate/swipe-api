export type Day = 0 | 1 | 2 | 3 | 4 | 5 | 6;

export interface WorkingHours {
  day: Day;
  isWorking: boolean;
  from: number;
  to: number;
}
