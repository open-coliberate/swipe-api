import * as moment from 'moment';

export interface MomentIntervalInterface {
  amount: moment.DurationInputArg1;
  units: moment.DurationInputArg2;
}
