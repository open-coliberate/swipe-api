import { ApiModelProperty } from '@nestjs/swagger';
export abstract class BaseResponse {
  @ApiModelProperty()
  message: string;
  @ApiModelProperty()
  errors: [];
}
