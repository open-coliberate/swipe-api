import {
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Expose, Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export abstract class CoreEntity {
  constructor(partial: Partial<CoreEntity>) {
    Object.assign(this, partial);
  }

  @ApiModelProperty({ readOnly: true, type: 'string', format: 'uuid' })
  @Expose()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiModelProperty({ readOnly: true, type: String, format: 'date-time' })
  @Expose()
  @Type(() => Date)
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @ApiModelProperty({ readOnly: true, type: String, format: 'date-time' })
  @Expose()
  @Type(() => Date)
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
