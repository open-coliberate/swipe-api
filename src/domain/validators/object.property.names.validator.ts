import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'Property names validator', async: false })
export class ObjectPropertyNamesValidator
  implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Names can be only [${validationArguments.constraints.join('|')}]`;
  }

  validate(
    data,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> | boolean {
    for (const key in data) {
      if (validationArguments.constraints.indexOf(key) === -1) {
        return false;
      }
    }
    return true;
  }
}
