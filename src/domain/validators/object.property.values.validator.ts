import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'Object property values validator', async: false })
export class ObjectPropertyValuesValidator
  implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Allowed values [${validationArguments.constraints.join('|')}]`;
  }

  validate(
    data,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> | boolean {
    for (const key in data) {
      if (validationArguments.constraints.indexOf(data[key]) === -1) {
        return false;
      }
    }
    return true;
  }
}
