import { TimeSlot } from '../interfaces/timeslot.interface';
import { WorkingHours } from '../../organization/entity/working.hours.entity';

// Checking allowed working time in space
export function isTimeslotAvailableInSpace(
  workingHours: WorkingHours[],
  eventDateDay: number,
  timeSlot: TimeSlot,
): TimeSlot | null {
  const spaceWorkingHoursForDay: Array<TimeSlot> = [];
  workingHours
    .filter(day => Number(day.day) === eventDateDay && day.isWorking)
    .sort((a, b) => a.from - b.from)
    .reduce((previousValue, currentValue) => {
      const intermediateObject = currentValue;
      if (previousValue.to === currentValue.from) {
        intermediateObject.from = previousValue.from;
      }
      spaceWorkingHoursForDay.push({
        from: intermediateObject.from,
        to: intermediateObject.to,
      });
      return intermediateObject;
    });

  return spaceWorkingHoursForDay
    ? spaceWorkingHoursForDay
        .filter(hours => hours.from <= timeSlot.from && hours.to >= timeSlot.to)
        .shift()
    : null;
}
