import { HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Event } from './event.entity';
import { Space } from '../organization/entity/space.entity';
import { Tool } from '../tool/entity/tool.entity';
import { isEqual } from 'lodash';
import moment from 'moment';
import { EventNotFoundException } from './exceptions/event.not.found.exception';
import { EventDTO } from './dto/event.dto';
import { MomentIntervalInterface } from '../domain/interfaces/interval.interface';
import { RecurringTypes } from './enums/recurring.enum';
import { CannotCreateEventException } from './exceptions/cannot.create.event.exception';
import { DateTimeService } from '../services/date.time.service';
import { isTimeslotAvailableInSpace } from '../domain/validators/timeslot.validator';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Injectable()
export class EventService {
  constructor(
    private readonly dateService: DateTimeService,
    @InjectRepository(Event)
    private readonly eventRepo: Repository<Event>,
    @InjectRepository(Space)
    private readonly spaceRepo: Repository<Space>,
    @InjectRepository(Tool)
    private readonly toolsRepo: Repository<Tool>,
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
  ) {}

  async getAll(): Promise<Event[]> {
    return (await this.eventRepo.find({
      relations: ['tools', 'space'],
    })).map(item => {
      let bookingDayWorkingHours = [];
      if (
        item &&
        item.tools &&
        item.tools[0].controller &&
        item.tools[0].controller.space &&
        item.tools[0].controller.space.workingHours
      ) {
        const bookingDayNumber = moment(item.eventDate).day();
        bookingDayWorkingHours = item.tools[0].controller.space.workingHours.filter(
          workingDay => {
            return bookingDayNumber === workingDay.day && workingDay.isWorking;
          },
        );
      }
      item.eventDayWorkingHours = bookingDayWorkingHours;
      return item;
    });
  }
  getById(id: string): Promise<Event> {
    try {
      return this.eventRepo.findOneOrFail(id);
    } catch (e) {
      switch (e.name) {
        case 'EntityNotFound': {
          throw new EventNotFoundException(`Event: ${id} is not found`);
        }
        default:
          throw e;
      }
    }
  }
  async create(eventDTO: EventDTO): Promise<Event[]> {
    let endDaysAmount: MomentIntervalInterface = { amount: 6, units: 'month' };
    let stepUnits: MomentIntervalInterface = { amount: 0, units: 'days' };
    const createdEvents = [];
    switch (eventDTO.recurring) {
      case RecurringTypes.PER_WEEK: {
        stepUnits = { amount: 7, units: 'days' };
        break;
      }
      case RecurringTypes.PER_MONTH: {
        stepUnits = { amount: 1, units: 'month' };
        break;
      }
      case RecurringTypes.ONE_TIME: {
        endDaysAmount = { amount: 0, units: 'days' };
        break;
      }
      default:
        throw new CannotCreateEventException('Invalid event recurring type');
    }

    const start = moment(eventDTO.eventDate);
    const end = start.clone().add(endDaysAmount.amount, endDaysAmount.units);
    const datesForEventCreating = this.dateService.getWeekDaysBetweenDatesWithSteps(
      start,
      end,
      stepUnits,
    );
    for (const eventWithDate of datesForEventCreating) {
      const timeSlot = eventDTO.timeSlot,
        eventDate = eventWithDate.toDate(),
        eventDateDay = eventDate.getDay();
      // Before event creating - we should check event date - may be another events has created meanwhile
      const bookedEvent = await this.eventRepo
        .createQueryBuilder('event')
        .where('time_slot @> :timeSlot', { timeSlot })
        .andWhere('date(event_date) = :dateEvent', {
          dateEvent: eventWithDate.toDate(),
        })
        .andWhere('"event"."spaceId" = :space_id', {
          space_id: eventDTO.spaceId,
        })
        .getOne();
      if (bookedEvent) {
        throw new CannotCreateEventException(
          `Cannot create an event: selected date and time slot already locked by: ${
            bookedEvent.title
          } [${bookedEvent.id}]`,
        );
      }
      const [tools, space] = await Promise.all([
        this.toolsRepo.findByIds(eventDTO.toolIds, {
          relations: ['bookings', 'controller'],
        }),
        this.spaceRepo.findOneOrFail(eventDTO.spaceId),
      ]);
      if (!space.workingHours) {
        throw new CannotCreateEventException(
          `Space ${space.title} doesn't have working hours`,
        );
      }
      const bookedTools = [];

      // Checking allowed working time in space
      if (
        !isTimeslotAvailableInSpace(
          await this.workingHoursRepo.find({
            where: {
              space: { id: space.id },
              day: eventDateDay,
            },
          }),
          eventDateDay,
          timeSlot,
        )
      ) {
        throw new CannotCreateEventException(
          `Tools can not be booked because requested time is not allowed working time in space ${
            space.title
          }`,
        );
      }

      for (const tool of tools) {
        for (const booking of tool.bookings) {
          if (
            isEqual(booking.timeSlot, eventDTO.timeSlot) &&
            eventWithDate.isSame(booking.bookingDate)
          ) {
            bookedTools.push({
              tool: {
                id: tool.id,
                title: tool.title,
              },
              booking: booking.id,
            });
          }
        }
      }
      if (bookedTools.length) {
        throw new CannotCreateEventException(
          `Cannot create an event: following tools [${bookedTools
            .map(booked => (booked.tool ? booked.tool.title : null))
            .join(', ')}] already used in bookings: [${bookedTools.map(booked =>
            booked.booking ? booked.booking : null,
          )}] `,
          HttpStatus.FORBIDDEN,
          bookedTools,
        );
      }

      const event = this.eventRepo.create(eventDTO);
      event.eventDate = eventDate;
      event.space = await this.spaceRepo.findOneOrFail(eventDTO.spaceId);
      event.tools = tools.filter(
        tool =>
          tool.controller &&
          tool.controller.space &&
          tool.controller.space.id === event.space.id,
      );

      createdEvents.push(this.eventRepo.save(event));
    }
    return Promise.all(createdEvents);
  }
}
