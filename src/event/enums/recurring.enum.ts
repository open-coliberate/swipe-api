export enum RecurringTypes {
  'PER_WEEK' = 'per_week',
  'PER_MONTH' = 'per_month',
  'ONE_TIME' = 'one_time',
}
