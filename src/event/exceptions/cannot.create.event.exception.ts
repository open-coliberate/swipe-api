import { HttpStatus } from '@nestjs/common';
import { BaseException } from '../../domain/exceptions/base.exception';

export class CannotCreateEventException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status = HttpStatus.FORBIDDEN;
  }
}
