import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';
import { Event } from './event.entity';
import { EventService } from './event.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { OnlyRoles } from '../auth/decorators/roles.decorator';
import { EventDTO } from './dto/event.dto';
import { Roles } from '../auth/enums/roles.enum';

@UseGuards(JwtAuthGuard)
@ApiUseTags('events')
@Controller('events')
export class EventController {
  constructor(private readonly eventService: EventService) {}

  @ApiOperation({
    title: 'Getting events',
  })
  @Get()
  @ApiResponse({ status: HttpStatus.OK, type: [EventDTO] })
  @ApiBearerAuth()
  async getAll(): Promise<Event[]> {
    return await this.eventService.getAll();
  }

  @ApiOperation({
    title: 'Getting event by id',
  })
  @Get(':id')
  @ApiResponse({ status: HttpStatus.OK, type: EventDTO })
  @ApiBearerAuth()
  async getById(@Param('id') id: string): Promise<Event> {
    return this.eventService.getById(id);
  }

  @ApiOperation({
    title: 'Creating event',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @Post()
  @ApiResponse({ status: HttpStatus.OK, type: EventDTO })
  @ApiBearerAuth()
  async create(@Body() eventDTO: EventDTO): Promise<Event[]> {
    return this.eventService.create(eventDTO);
  }
}
