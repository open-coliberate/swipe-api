import { CoreEntity } from '../domain/entity/core.entity';
import { Exclude, Expose, Transform } from 'class-transformer';
import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import { Space } from '../organization/entity/space.entity';
import { Tool } from '../tool/entity/tool.entity';
import { TimeSlot } from '../domain/interfaces/timeslot.interface';
import { RecurringTypes } from './enums/recurring.enum';
import { WorkingHours } from '../domain/interfaces/working.hours.interface';

@Exclude()
@Entity('events')
export class Event extends CoreEntity {
  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  title: string;

  @Index()
  @Expose()
  @Transform((eventDate: Date) => {
    eventDate.setHours(0, 0, 0);
    return eventDate;
  })
  @Column({
    name: 'event_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  eventDate: Date;

  @Index()
  @Expose()
  @Column({
    name: 'time_slot',
    type: 'jsonb',
    nullable: false,
  })
  timeSlot: TimeSlot;

  @ApiModelProperty({ readOnly: true, type: [Space] })
  @Expose()
  @ManyToOne(type => Space, space => space.events, {
    cascade: false,
    eager: false,
    nullable: false,
  })
  space: Space;

  @Expose()
  @ManyToMany(type => Tool, tool => tool.events)
  @JoinTable({ name: 'events_tools' })
  tools: Tool[];

  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
    default: RecurringTypes.ONE_TIME,
  })
  recurring: RecurringTypes = RecurringTypes.ONE_TIME;

  @Expose()
  eventDayWorkingHours?: WorkingHours[];
}
