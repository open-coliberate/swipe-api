import {
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsString,
  IsUUID,
} from 'class-validator';
import { IsSpaceExists } from '../../organization/validators/is.space.exists';
import { IsTimeSlot } from '../../booking/validators/is.timeslot.validator';
import { RecurringTypes } from '../enums/recurring.enum';
import { TimeSlot } from '../../domain/interfaces/timeslot.interface';
import { ApiModelProperty } from '@nestjs/swagger';

export class EventDTO {
  @ApiModelProperty({
    type: 'string',
    required: true,
  })
  @IsString()
  title: string;

  @ApiModelProperty({
    type: 'string',
    format: 'date-time',
    required: true,
  })
  @IsDateString()
  eventDate: Date;

  @ApiModelProperty({
    type: 'string',
    format: 'date-time',
    required: true,
  })
  @IsTimeSlot()
  timeSlot: TimeSlot;

  @ApiModelProperty({
    type: 'string',
    format: 'uuid',
    required: true,
  })
  @IsUUID()
  @IsSpaceExists()
  spaceId: string;

  @ApiModelProperty({
    type: 'array',
    format: 'uuid',
    required: true,
  })
  @IsUUID('4', { each: true })
  @IsNotEmpty()
  toolIds: string[];

  @ApiModelProperty({
    type: 'string',
    required: true,
    enum: RecurringTypes,
  })
  @IsEnum(RecurringTypes)
  recurring: RecurringTypes;
}
