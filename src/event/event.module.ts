import { Module } from '@nestjs/common';
import { EventController } from './event.controller';
import { EventService } from './event.service';
import { Space } from '../organization/entity/space.entity';
import { Event } from './event.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tool } from '../tool/entity/tool.entity';
import { DateTimeService } from '../services/date.time.service';
import { WorkingHours } from '../organization/entity/working.hours.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Space, Event, Tool, WorkingHours])],
  controllers: [EventController],
  providers: [EventService, DateTimeService],
})
export class EventModule {}
