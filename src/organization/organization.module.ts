import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organization } from './entity/organization.entity';
import { Hub } from './entity/hub.entity';
import { Space } from './entity/space.entity';
import { Tool } from '../tool/entity/tool.entity';
import { IsHubInOrganizationValidConstraint } from './validators/is.hub.in.organization.valid';
import { OrganizationService } from './services/organization.service';
import { HubService } from './services/hub.service';
import { SpaceService } from './services/space.service';
import { OrganizationController } from './controllers/organization.controller';
import { HubController } from './controllers/hub.controller';
import { SpaceController } from './controllers/space.controller';
import { RfidService } from '../rfid/rfid.service';
import { RFIDCard } from '../rfid/entity/rfid.card.entity';
import { RFIDController } from '../rfid/entity/rfid.controller.entity';
import { UserService } from '../user/user.service';
import { User } from '../user/entity/user.entity';
import { ResetPasswordRequest } from '../auth/entity/reset.password.request.entity';
import { ToolTemplate } from '../tool/entity/tool.template.entity';
import { MailerService } from '../mailer/mailer.service';
import { WorkingHours } from './entity/working.hours.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Organization,
      Hub,
      Space,
      Tool,
      RFIDCard,
      RFIDController,
      User,
      ResetPasswordRequest,
      ToolTemplate,
      WorkingHours,
    ]),
  ],
  providers: [
    OrganizationService,
    HubService,
    SpaceService,
    IsHubInOrganizationValidConstraint,
    RfidService,
    UserService,
    MailerService,
  ],
  controllers: [OrganizationController, HubController, SpaceController],
})
export class OrganizationModule {}
