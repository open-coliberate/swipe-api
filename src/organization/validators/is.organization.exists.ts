import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { OrganizationService } from '../services/organization.service';

@ValidatorConstraint({ name: 'IsOrganizationExist', async: true })
@Injectable()
export class IsOrganizationExistConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('OrganizationService')
    private readonly organizationService: OrganizationService,
  ) {}
  async validate(id: string, validationArguments: ValidationArguments) {
    try {
      const organization = await this.organizationService.findOneById(id);
      return !!organization;
    } catch (e) {
      Logger.error(
        `IsOrganizationExistConstraint has failed with error: ${e.message}`,
      );
      return false;
    }
  }
  defaultMessage(args: ValidationArguments) {
    return `Organization ${args.value} doesn't exists.`;
  }
}

export function IsOrganizationExist(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsOrganizationExistConstraint,
    });
  };
}
