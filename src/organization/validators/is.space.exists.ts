import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { SpaceService } from '../services/space.service';

@ValidatorConstraint({ name: 'IsSpaceExists', async: true })
@Injectable()
export class IsSpaceExistConstraint implements ValidatorConstraintInterface {
  constructor(
    @Inject('SpaceService')
    private readonly spaceService: SpaceService,
  ) {}
  async validate(id: string, validationArguments: ValidationArguments) {
    try {
      const space = await this.spaceService.findOneById(id);
      return !!space;
    } catch (e) {
      Logger.error(
        `IsSpaceExistConstraint has failed with error: ${e.message}`,
      );
      return false;
    }
  }
  defaultMessage(args: ValidationArguments) {
    return `Space ${args.value} doesn't exists.`;
  }
}

export function IsSpaceExists(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsSpaceExistConstraint,
    });
  };
}
