import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Inject } from '@nestjs/common';
import { HubService } from '../services/hub.service';
import { RfidControllerInitInNewHubConfirmation } from '../../rfid/dto/rfid.controller.init.in.new.hub.confirmation';
import { UserService } from '../../user/user.service';
import { validateUserOrganizationAndCard } from '../../user/validators/controller.init.flow';

@ValidatorConstraint({ name: 'IsHubExistsValidator', async: true })
export class IsHubInOrganizationValidConstraint
  implements ValidatorConstraintInterface {
  constructor(
    @Inject('UserService') private readonly userService: UserService,
    @Inject('HubService') private readonly hubService: HubService,
  ) {}

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `User with this email and RFID card have not access to this hub or not exists`;
  }

  async validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    const payload = validationArguments.object as RfidControllerInitInNewHubConfirmation;
    try {
      const [user, hub] = await Promise.all([
        this.userService.findOneByEmail(payload.labManagerEmail, {
          relations: ['rfidCard'],
        }),
        this.hubService.findOneById(payload.hubId),
      ]);
      return await validateUserOrganizationAndCard(
        user,
        hub.organization,
        payload.RFIDCardCode,
      );
    } catch (e) {
      switch (e.name) {
        case 'HubNotFoundException':
          return false;
      }
      throw e;
    }
  }
}

export function IsHubInOrganizationValid(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsHubInOrganizationValidConstraint,
    });
  };
}
