import {
  Controller,
  UseGuards,
  HttpStatus,
  Get,
  Patch,
  Body,
  Param,
  BadRequestException,
  Delete,
  NotFoundException,
  Post,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { Hub } from '../entity/hub.entity';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { HubService } from '../services/hub.service';
import { HubDto } from '../dto/hub.dto';
import { OnlyRoles } from '../../auth/decorators/roles.decorator';
import { BindToOrganizationDto } from '../dto/bind.to.organization.dto';
import { Roles } from '../../auth/enums/roles.enum';

@UseGuards(JwtAuthGuard)
@ApiUseTags('hubs')
@Controller('hubs')
export class HubController {
  constructor(private readonly hubService: HubService) {}

  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Get()
  @ApiResponse({ status: HttpStatus.OK, type: [HubDto] })
  @ApiBearerAuth()
  getAll(): Promise<Hub[]> {
    return this.hubService.findAll();
  }

  @ApiResponse({ status: HttpStatus.CREATED, type: Hub })
  @ApiBearerAuth()
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Post()
  async create(@Body() hubDto: HubDto): Promise<Hub> {
    try {
      return await this.hubService.create(hubDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @OnlyRoles(Roles.GENERAL_USER)
  @Get(':id')
  @ApiResponse({ status: HttpStatus.OK, type: HubDto })
  @ApiBearerAuth()
  async get(@Param('id') id: string): Promise<Hub> {
    try {
      return await this.hubService.findOneById(id);
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Patch(':id')
  @ApiResponse({ status: HttpStatus.OK, type: HubDto })
  @ApiBearerAuth()
  async update(@Body() hubDto: HubDto, @Param('id') id: string): Promise<Hub> {
    try {
      return await this.hubService.update(id, hubDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @OnlyRoles(Roles.OWNER, Roles.OWNER)
  @Delete(':id')
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  @ApiBearerAuth()
  async delete(@Param('id') id: string) {
    try {
      await this.hubService.delete(id);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Patch('/bind-to-organization/:id')
  @ApiResponse({ status: HttpStatus.OK, type: HubDto })
  @ApiBearerAuth()
  async bindToOrganization(
    @Body() bindHubToOrganizationDto: BindToOrganizationDto,
    @Param('id') id: string,
  ): Promise<Hub> {
    try {
      return await this.hubService.bindToOrganization(
        id,
        bindHubToOrganizationDto.organizationId,
      );
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Patch('/unbind-from-organization/:id')
  @ApiResponse({ status: HttpStatus.OK, type: HubDto })
  @ApiBearerAuth()
  async unBindFromOrganization(
    @Body() unBindHubFromOrganizationDto: BindToOrganizationDto,
    @Param('id') id: string,
  ): Promise<Hub> {
    try {
      return await this.hubService.unBindFromOrganization(
        id,
        unBindHubFromOrganizationDto.organizationId,
      );
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
}
