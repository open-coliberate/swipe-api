import {
  Controller,
  UseGuards,
  HttpStatus,
  Get,
  Patch,
  Body,
  Param,
  BadRequestException,
  Delete,
  NotFoundException,
  Post,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';
import { Space } from '../entity/space.entity';
import { WorkingHours } from '../../domain/interfaces/working.hours.interface';
import { BaseResponse } from '../../domain/responses/base.response';
import { HubDto } from '../dto/hub.dto';
import { OnlyRoles } from '../../auth/decorators/roles.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { ParseDatePipe } from '../../application/pipes/parse.date.pipe';
import { BindToHubDto } from '../dto/bind.to.hub.dto';
import { Roles } from '../../auth/enums/roles.enum';
import { SpaceDto } from '../dto/space.dto';
import { SpaceService } from '../services/space.service';

@UseGuards(JwtAuthGuard)
@ApiUseTags('spaces')
@Controller('spaces')
export class SpaceController {
  constructor(private readonly spaceService: SpaceService) {}

  @ApiOperation({
    title: 'Get all spaces list',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @Get()
  @ApiResponse({ status: HttpStatus.OK, type: [BaseResponse] })
  @ApiBearerAuth()
  getAll(): Promise<Space[]> {
    return this.spaceService.findAll();
  }

  @ApiOperation({
    title: 'Create space',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.SUPER_USER, Roles.OWNER)
  @Post()
  @ApiResponse({ status: HttpStatus.CREATED, type: BaseResponse })
  @ApiBearerAuth()
  async create(@Body() spaceDto: SpaceDto): Promise<Space> {
    try {
      return await this.spaceService.create(spaceDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @ApiOperation({
    title: 'Get space by id',
  })
  @OnlyRoles(Roles.GENERAL_USER)
  @Get(':id')
  @ApiResponse({ status: HttpStatus.OK, type: BaseResponse })
  @ApiBearerAuth()
  async get(@Param('id') id: string): Promise<Space> {
    try {
      return await this.spaceService.findOneById(id);
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  @ApiOperation({
    title: 'Get available time slots per day',
  })
  @Get(':id/available-time-slots-per-date/:date')
  @ApiResponse({ status: HttpStatus.OK, type: BaseResponse })
  @ApiBearerAuth()
  getAvailableTimeSlotsPerDate(
    @Param('id') id: string,
    @Param('date', new ParseDatePipe()) date: Date,
  ): Promise<WorkingHours[]> {
    return this.spaceService.getAvailableTimeSlotsByDay(id, date.getDay());
  }

  @ApiOperation({
    title: 'Edit space',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER)
  @Patch(':id')
  @ApiResponse({ status: HttpStatus.OK, type: BaseResponse })
  @ApiBearerAuth()
  async update(
    @Body() spaceDto: SpaceDto,
    @Param('id') id: string,
  ): Promise<Space> {
    try {
      return await this.spaceService.update(id, spaceDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @ApiOperation({
    title: 'Delete space',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER, Roles.SUPER_USER)
  @Delete(':id')
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  @ApiBearerAuth()
  async delete(@Param('id') id: string) {
    try {
      await this.spaceService.delete(id);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @ApiOperation({
    title: 'Bind hub to space',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER, Roles.SUPER_USER)
  @Patch('/bind-to-hub/:id')
  @ApiResponse({ status: HttpStatus.OK, type: BaseResponse })
  @ApiBearerAuth()
  async bindToOrganization(
    @Body() bindHubtoDto: BindToHubDto,
    @Param('id') id: string,
  ): Promise<Space> {
    try {
      return await this.spaceService.bindToHub(id, bindHubtoDto.hubId);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @ApiOperation({
    title: 'Unbind hub to space',
  })
  @OnlyRoles(Roles.LAB_MANAGER, Roles.OWNER, Roles.SUPER_USER)
  @Patch('/unbind-from-hub/:id')
  @ApiResponse({ status: HttpStatus.OK, type: HubDto })
  @ApiBearerAuth()
  async unBindFromOrganization(
    @Body() unBindHubFromDto: BindToHubDto,
    @Param('id') id: string,
  ): Promise<Space> {
    try {
      return await this.spaceService.unBindFromHub(id, unBindHubFromDto.hubId);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
}
