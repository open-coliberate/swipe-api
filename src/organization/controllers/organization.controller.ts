import {
  Controller,
  UseGuards,
  HttpStatus,
  Get,
  Patch,
  Body,
  Param,
  BadRequestException,
  Delete,
  NotFoundException,
  Post,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { Organization } from '../entity/organization.entity';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { OrganizationService } from '../services/organization.service';
import { OnlyRoles } from '../../auth/decorators/roles.decorator';
import { Roles } from '../../auth/enums/roles.enum';
import { OrganizationDto } from '../dto/organization.dto';

@Controller('organizations')
@UseGuards(JwtAuthGuard)
@ApiUseTags('organizations')
export class OrganizationController {
  constructor(private readonly organizationService: OrganizationService) {}

  @OnlyRoles(Roles.OWNER)
  @Get('/')
  @ApiResponse({ status: HttpStatus.OK, type: [OrganizationDto] })
  @ApiBearerAuth()
  getAll(): Promise<Organization[]> {
    return this.organizationService.findAll();
  }

  @ApiResponse({ status: HttpStatus.CREATED, type: Organization })
  @ApiBearerAuth()
  @OnlyRoles(Roles.OWNER)
  @Post()
  async create(@Body() organizationDto: OrganizationDto) {
    try {
      return await this.organizationService.create(organizationDto);
    } catch (e) {
      throw new BadRequestException(
        `Organization ${organizationDto.title} cannot be created!`,
      );
    }
  }

  @Get(':id')
  @ApiResponse({ status: HttpStatus.OK, type: OrganizationDto })
  @ApiBearerAuth()
  async get(@Param('id') id: string): Promise<Organization> {
    try {
      return await this.organizationService.findOneById(id);
    } catch (e) {
      throw new NotFoundException(`Organization ${id} cannot be found!`);
    }
  }

  @OnlyRoles(Roles.OWNER)
  @Patch(':id')
  @ApiResponse({ status: HttpStatus.OK, type: OrganizationDto })
  @ApiBearerAuth()
  async update(
    @Body() organizationDto: OrganizationDto,
    @Param('id') id: string,
  ): Promise<Organization> {
    try {
      return await this.organizationService.update(id, organizationDto);
    } catch (e) {
      throw new BadRequestException(`Organization ${id} cannot be updated!`);
    }
  }

  @OnlyRoles(Roles.OWNER)
  @Delete(':id')
  @ApiResponse({ status: HttpStatus.NO_CONTENT })
  @ApiBearerAuth()
  async delete(@Param('id') id: string): Promise<void> {
    try {
      await this.organizationService.delete(id);
    } catch (e) {
      throw new BadRequestException(`Organization ${id} cannot be deleted!`);
    }
  }
}
