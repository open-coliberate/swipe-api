import { CoreEntity } from '../../domain/entity/core.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import {
  Day,
  WorkingHours as WorkingHoursInterface,
} from '../../domain/interfaces/working.hours.interface';
import { Space } from './space.entity';
import { User } from '../../user/entity/user.entity';
import { Transform } from 'class-transformer';

export const DayEnum = [0, 1, 2, 3, 4, 5, 6];

@Entity('working_hours')
export class WorkingHours extends CoreEntity implements WorkingHoursInterface {
  @Column({
    type: 'smallint',
    unsigned: true,
    enum: DayEnum,
  })
  day: Day;

  @Column({
    type: 'int',
  })
  from: number;

  @Column({
    type: 'int',
  })
  to: number;

  @Column({
    type: 'bool',
  })
  isWorking: boolean;

  @ManyToOne(o => Space, {
    nullable: false,
  })
  space: Space;

  @ManyToOne(o => User, {
    nullable: false,
  })
  user: User;
}
