import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { CoreEntity } from '../../domain/entity/core.entity';
import { Hub } from './hub.entity';
import { Event } from '../../event/event.entity';
import { WorkingHours } from './working.hours.entity';

@Exclude()
@Entity('spaces')
export class Space extends CoreEntity {
  @Expose()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  title: string;

  @Expose()
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Expose()
  @ManyToOne(type => Hub, hub => hub.spaces, {
    onDelete: 'SET NULL',
    eager: true,
  })
  hub: Hub;

  @Expose()
  @OneToMany(type => Event, event => event.space)
  events: Event[];

  @Expose()
  @OneToMany(type => WorkingHours, workingHours => workingHours.space, {
    onDelete: 'CASCADE',
    eager: true,
  })
  workingHours: WorkingHours[];
}
