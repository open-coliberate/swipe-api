import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { Exclude, Expose, Transform } from 'class-transformer';
import { CoreEntity } from '../../domain/entity/core.entity';
import { Organization } from './organization.entity';
import { Space } from './space.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
@Entity('hubs')
export class Hub extends CoreEntity {
  @ApiModelProperty({ type: 'string' })
  @Expose()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  title: string;

  @Expose()
  @Column({
    type: 'varchar',
    nullable: true,
  })
  address: string;

  @Transform(organization => {
    return organization
      ? {
          id: organization.id,
          title: organization.title,
        }
      : null;
  })
  @ManyToOne(type => Organization, organization => organization.hubs, {
    onDelete: 'SET NULL',
    eager: true,
  })
  @Expose()
  organization: Organization;

  @Expose()
  @OneToMany(type => Space, space => space.hub)
  spaces: Space[];
}
