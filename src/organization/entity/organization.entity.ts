import { Column, Entity, OneToMany } from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Hub } from './hub.entity';
import { User } from '../../user/entity/user.entity';
import { CoreEntity } from '../../domain/entity/core.entity';
import { ToolTemplate } from '../../tool/entity/tool.template.entity';

@Exclude()
@Entity('organizations')
export class Organization extends CoreEntity {
  @Expose()
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  title: string;

  @Expose()
  @OneToMany(type => Hub, hub => hub.organization)
  hubs: Hub[];

  @Expose()
  @OneToMany(type => User, user => user.organization)
  users: User[];

  @Expose()
  @OneToMany(type => ToolTemplate, toolTemplate => toolTemplate.organization)
  toolTemplates: ToolTemplate[];
}
