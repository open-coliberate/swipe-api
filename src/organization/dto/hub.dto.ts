import { IsNotEmpty, IsUUID, ValidateIf } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { Organization } from '../entity/organization.entity';

export class HubDto {
  @ValidateIf(o => {
    return o.hasOwnProperty('title');
  })
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'Hub name',
    required: true,
  })
  title: string;

  @ValidateIf(o => {
    return o.hasOwnProperty('address');
  })
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'Hub address',
    required: false,
  })
  address?: string;

  @ValidateIf(o => {
    return o.hasOwnProperty('organization');
  })
  @IsNotEmpty()
  @IsUUID()
  @ApiModelProperty({
    description: 'Organization UUID',
    required: true,
  })
  organization?: Organization;
}
