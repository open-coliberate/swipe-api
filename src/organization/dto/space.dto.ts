import {
  IsNotEmpty,
  IsUUID,
  ArrayMaxSize,
  IsArray,
  ValidateIf,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { WorkingHours } from '../../domain/interfaces/working.hours.interface';

export class SpaceDto {
  @ValidateIf(o => {
    return o.hasOwnProperty('title');
  })
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'Space name',
    required: true,
  })
  title: string;

  @ValidateIf(o => {
    return o.hasOwnProperty('hubId');
  })
  @IsNotEmpty()
  @IsUUID('4')
  @ApiModelProperty({
    description: 'Hub UUID',
    required: true,
  })
  hubId: string;

  @ValidateIf(o => {
    return o.hasOwnProperty('description');
  })
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'Space description',
    required: false,
  })
  description?: string;

  @ValidateIf(o => {
    return o.hasOwnProperty('workingHours');
  })
  @IsNotEmpty()
  @IsArray()
  @ArrayMaxSize(7)
  @ApiModelProperty({
    description: 'Space working hours',
    required: false,
    isArray: true,
  })
  workingHours: WorkingHours[];
}
