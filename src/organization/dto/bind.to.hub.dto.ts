import { IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class BindToHubDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiModelProperty({
    description: 'Hub UUID',
    required: true,
  })
  hubId: string;
}
