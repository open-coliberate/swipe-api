import { IsNotEmpty, ValidateIf } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class OrganizationDto {
  @ValidateIf(o => {
    return o.hasOwnProperty('title');
  })
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'Organization name',
    required: true,
  })
  title: string;
}
