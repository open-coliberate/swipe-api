import { IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class BindToOrganizationDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiModelProperty({
    description: 'Organization UUID',
    required: true,
  })
  organizationId: string;
}
