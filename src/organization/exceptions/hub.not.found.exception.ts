import { HttpStatus } from '@nestjs/common';
import { BaseException } from '../../domain/exceptions/base.exception';

export class HubNotFoundException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status = HttpStatus.NOT_FOUND;
  }
}
