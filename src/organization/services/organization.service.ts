import { Injectable } from '@nestjs/common';
import { Organization } from '../entity/organization.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrganizationDto } from '../dto/organization.dto';
import { OrganizationNotFoundException } from '../exceptions/organization.not.found.exception';
@Injectable()
export class OrganizationService {
  constructor(
    @InjectRepository(Organization)
    private readonly organizationRepository: Repository<Organization>,
  ) {}

  async findAll(): Promise<Organization[]> {
    return await this.organizationRepository.find();
  }

  async create(organizationCardDto: OrganizationDto) {
    return await this.organizationRepository.save(organizationCardDto);
  }

  async update(
    id: string,
    organizationDto: OrganizationDto,
  ): Promise<Organization> {
    try {
      await this.organizationRepository.update(id, organizationDto);
      return await this.findOneById(id);
    } catch (error) {
      throw new OrganizationNotFoundException(
        `Organization with ${id} is not found!`,
      );
    }
  }
  async delete(id: string): Promise<void> {
    try {
      await this.organizationRepository.delete(id);
    } catch (error) {
      throw new OrganizationNotFoundException(
        `Organization with ${id} is not found!`,
      );
    }
  }

  async findOneById(id: string): Promise<Organization> {
    try {
      return await this.organizationRepository.findOneOrFail(id);
    } catch (error) {
      throw new OrganizationNotFoundException(
        `Organization with ${id} is not found!`,
      );
    }
  }

  async findOneByTitle(title: string): Promise<Organization> {
    try {
      return await this.organizationRepository.findOneOrFail({ title });
    } catch (error) {
      throw new OrganizationNotFoundException(
        `Organization with title ${title} is not found!`,
      );
    }
  }

  async save(organization: Organization): Promise<Organization> {
    return await this.organizationRepository.save(organization);
  }
}
