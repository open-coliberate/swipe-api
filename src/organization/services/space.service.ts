import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Space } from '../entity/space.entity';
import { HubService } from './hub.service';
import { SpaceDto } from '../dto/space.dto';
import { HubNotFoundException } from '../exceptions/hub.not.found.exception';
import { SpaceNotFoundException } from '../exceptions/space.not.found.exception';
import { SpaceHubMismatch } from '../exceptions/space.hub.mismatch.exception';
import { Hub } from '../entity/hub.entity';
import { WorkingHours } from '../entity/working.hours.entity';

@Injectable()
export class SpaceService {
  constructor(
    @InjectRepository(Space)
    private readonly spaceRepository: Repository<Space>,
    @InjectRepository(WorkingHours)
    private readonly workingHoursRepo: Repository<WorkingHours>,
    private readonly hubService: HubService,
  ) {}

  async findAll(): Promise<Space[]> {
    return await this.spaceRepository.find();
  }

  async create(spaceDto: SpaceDto): Promise<Space> {
    const hub = await this.hubService.findOneById(spaceDto.hubId);
    if (!hub) {
      throw new HubNotFoundException(
        `Hub with id: ${spaceDto.hubId} is not found!`,
      );
    }
    const space = await this.spaceRepository.create(spaceDto);
    space.hub = hub;
    return await this.spaceRepository.save(space);
  }

  async getDefautlWithHubAndSpace(
    organizationId: string,
  ): Promise<[Hub, Space]> {
    const hub = await this.hubService.getDefaultOne(organizationId);
    const space = await this.getDefaultOne(hub);
    return [hub, space];
  }

  async getDefaultOne(hub: Hub): Promise<Space> {
    let space = await this.spaceRepository.findOne({
      where: { hub: hub.id, title: 'Default space' },
    });
    if (!space) {
      space = await this.spaceRepository.save(
        this.spaceRepository.create({
          title: 'Default space',
          hub,
        }),
      );
    }
    return space;
  }

  async update(id: string, spaceDto: SpaceDto): Promise<Space> {
    try {
      await this.spaceRepository.update(id, spaceDto);
      return await this.findOneById(id);
    } catch (error) {
      throw new SpaceNotFoundException(error.message);
    }
  }
  async delete(id: string): Promise<void> {
    await this.spaceRepository.delete(id);
  }

  async findOneById(id: string): Promise<Space> {
    try {
      return await this.spaceRepository.findOneOrFail(id);
    } catch (error) {
      throw new SpaceNotFoundException(`Space with ${id} is not found!`);
    }
  }

  async findOne(payload: any): Promise<Space> {
    try {
      return await this.spaceRepository.findOneOrFail(payload);
    } catch (error) {
      throw new SpaceNotFoundException(`Space is not found!`);
    }
  }

  async findOneByIdAndHubId(id: string, hubId: string): Promise<Space> {
    const space = await this.spaceRepository
      .createQueryBuilder('space')
      .innerJoinAndSelect('space.hub', 'hub', 'hub.id = :hubId', {
        hubId,
      })
      .where('space.id = :id', { id })
      .getOne();
    if (!space) {
      throw new SpaceNotFoundException(
        `Space with id ${id} and Hub ${hubId} is not found!`,
      );
    }

    return space;
  }

  async bindToHub(id: string, hubId: string): Promise<Space> {
    const hub = await this.hubService.findOneById(hubId);
    if (!hub) {
      throw new HubNotFoundException(`Hub with id: ${hubId} is not found!`);
    }
    const space = await this.findOneById(id);

    if (!space) {
      throw new SpaceNotFoundException(`Space with id: ${id} is not found!`);
    }
    space.hub = hub;

    return await this.spaceRepository.save(space);
  }

  async unBindFromHub(id: string, hubId: string): Promise<Space> {
    const hub = await this.hubService.findOneById(hubId);
    if (!hub) {
      throw new HubNotFoundException(`Hub with id: ${hubId} is not found!`);
    }
    const space = await this.findOneById(id);

    if (!space) {
      throw new SpaceNotFoundException(`Space with id: ${id} is not found`);
    }
    if (space.hub.id !== hub.id) {
      throw new SpaceHubMismatch(
        `Currently space with id: ${id} has different hub, than the one that was passed. Please pass correct hub id!`,
      );
    }
    space.hub = null;

    return await this.spaceRepository.save(space);
  }

  getAvailableTimeSlotsByDay(
    spaceId: string,
    day: number,
  ): Promise<WorkingHours[]> {
    return this.workingHoursRepo.find({
      where: {
        space: { id: spaceId },
        day,
      },
    });
  }
}
