import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrganizationService } from './organization.service';
import { Hub } from '../entity/hub.entity';
import { HubDto } from '../dto/hub.dto';
import { OrganizationNotFoundException } from '../exceptions/organization.not.found.exception';
import { HubNotFoundException } from '../exceptions/hub.not.found.exception';
import { BindToOrganizationDto } from '../dto/bind.to.organization.dto';
import { HubOrganizationMismatch } from '../exceptions/hub.organization.mismatch.exception';
import { Organization } from '../entity/organization.entity';

@Injectable()
export class HubService {
  constructor(
    @InjectRepository(Hub)
    private readonly hubRepository: Repository<Hub>,
    private readonly organizationService: OrganizationService,
  ) {}

  async findAll(): Promise<Hub[]> {
    return await this.hubRepository.find();
  }

  async create(hubDto: HubDto): Promise<Hub> {
    const organization = await this.organizationService.findOneById(
      hubDto.organization.id,
    );
    if (!organization) {
      throw new OrganizationNotFoundException(
        `Organization with id: ${hubDto.organization.id} is not found!`,
      );
    }
    const hub = await this.hubRepository.create(hubDto);
    hub.organization = organization;
    return await this.hubRepository.save(hub);
  }

  async update(id: string, hubDto: HubDto): Promise<Hub> {
    try {
      await this.hubRepository.update(id, hubDto);
      return await this.findOneById(id);
    } catch (error) {
      throw new HubNotFoundException(`Hub with ${id} is not found!`);
    }
  }
  async delete(id: string): Promise<void> {
    try {
      await this.hubRepository.delete(id);
    } catch (error) {
      throw new HubNotFoundException(error);
    }
  }

  async findOneById(id: string): Promise<Hub> {
    try {
      return await this.hubRepository.findOneOrFail(id);
    } catch (error) {
      throw new HubNotFoundException(`Hub with ${id} is not found!`);
    }
  }

  async findOneByIdAndOrganizationId(
    id: string,
    organizationId: string,
  ): Promise<Hub> {
    const space = await this.hubRepository
      .createQueryBuilder('hub')
      .innerJoinAndSelect(
        'hub.organization',
        'organization',
        'organization.id = :organizationId',
        {
          organizationId,
        },
      )
      .where('hub.id = :id', { id })
      .getOne();
    if (!space) {
      throw new HubNotFoundException(
        `Hub with id ${id} and Organization ${organizationId} is not found!`,
      );
    }

    return space;
  }

  async bindToOrganization(id: string, organizationId: string): Promise<Hub> {
    const organization = await this.organizationService.findOneById(
      organizationId,
    );
    if (!organization) {
      throw new HubNotFoundException(
        `Organization with id: ${organizationId} is not found!`,
      );
    }
    const hub = await this.findOneById(id);

    if (!hub) {
      throw new HubNotFoundException(
        `Hub with id: ${organizationId} is not found!`,
      );
    }
    hub.organization = organization;

    return await this.hubRepository.save(hub);
  }

  async unBindFromOrganization(
    id: string,
    organizationId: string,
  ): Promise<Hub> {
    const organization = await this.organizationService.findOneById(
      organizationId,
    );
    if (!organization) {
      throw new OrganizationNotFoundException(
        `Organization with id: ${organizationId} is not found!`,
      );
    }
    const hub = await this.findOneById(id);

    if (!hub) {
      throw new HubNotFoundException(
        `Hub with id: ${organizationId} is not found`,
      );
    }
    if (hub.organization.id !== organization.id) {
      throw new HubOrganizationMismatch(
        `Currently hub with id: ${organizationId} has different organization, than the one that was passed. Please pass correct organization id!`,
      );
    }
    hub.organization = null;

    return await this.hubRepository.save(hub);
  }

  async findByOrganizationTitle(organizationTitle: string): Promise<Hub[]> {
    return this.hubRepository
      .createQueryBuilder('hub')
      .innerJoinAndSelect(
        'hub.organization',
        'organization',
        'organization.title = :organizationTitle',
        {
          organizationTitle,
        },
      )
      .getMany();
  }

  async getDefaultOne(organizationId: string): Promise<Hub> {
    let hub = await this.hubRepository.findOne({
      where: { organization: organizationId, title: 'Default hub' },
    });
    if (!hub) {
      const organization = await this.organizationService.findOneById(
        organizationId,
      );
      hub = await this.create({
        title: 'Default hub',
        organization,
      });
    }
    return hub;
  }
}
