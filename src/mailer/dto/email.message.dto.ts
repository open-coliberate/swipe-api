export interface EmailMessageDto {
  to: string;
  subject: string;
  template: string;
  context: EmailMessageDtoContext;
}

interface EmailMessageDtoContext {
  firstName: string;
  lastName: string;
  token: string;
  hostName: string;
}