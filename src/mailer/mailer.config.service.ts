import {
  MailerModuleOptions,
  MailerOptionsFactory,
} from '@nest-modules/mailer';
import { ConfigService } from '../config/config.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailerConfigService implements MailerOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  createMailerOptions(): MailerModuleOptions {
    return this.configService.getMailerConfiguration();
  }
}
