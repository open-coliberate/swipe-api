import { Module } from '@nestjs/common';
import { MailerConfigService } from './mailer.config.service';
import { MailerModule } from '@nest-modules/mailer';
import { MailerService } from './mailer.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useClass: MailerConfigService,
    }),
  ],
  providers: [MailerService],
})
export class ApplicationMailerModule {}
