import { ApplicationContext } from '../../application/app.context';
import { MailerProvider } from '@nest-modules/mailer';
import { Job } from 'bull';
import { Logger } from '@nestjs/common';
import { inspect } from 'util';

const MailerSendProcessor = async (job: Job) => {
  const context = await ApplicationContext();
  const mailerService = context.get(MailerProvider);
  Logger.log(`Processing email message job started: ${inspect(job.data)}`);

  try {
    await mailerService.sendMail(job.data);
  } catch (error) {
    Logger.error(`Error during email sending: $(error.trace)`);
  }
};
export { MailerSendProcessor };
