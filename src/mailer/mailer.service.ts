import { Injectable } from '@nestjs/common';
import { MailerProvider } from '@nest-modules/mailer';
import { MessageType, MessageTypes } from './constants/mailer.constants';
import { InjectQueue } from 'nest-bull';
import { Job, Queue } from 'bull';
import { EmailMessageDto } from './dto/email.message.dto';

@Injectable()
export class MailerService {
  constructor(
    private readonly mailerService: MailerProvider, // @InjectQueue('emails') readonly queue: Queue,
  ) {}

  async addMessageForProcessing(message: EmailMessageDto): Promise<Job> {
    return await this.sendEmail(message);
  }

  // async processMessage(messageId: string) {
  //   const message = await this.queue.getJob(messageId);
  //
  //   return await this.sendEmail(message.data);
  // }

  async sendEmail(message: EmailMessageDto) {
    return await this.mailerService.sendMail(message);
  }

  getMessage(mailTo: string, type: MessageType, context: any): EmailMessageDto {
    const messageTypeMeta = MessageTypes[type];
    return {
      to: mailTo,
      subject: messageTypeMeta.subject,
      template: messageTypeMeta.template,
      context,
    };
  }
}
