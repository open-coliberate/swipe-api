export enum MessageType {
  ResetPassword = 'ResetPassword',
  SignUp = 'SignUp',
  InitInNewHub = 'InitInNewHub',
}

export const MessageTypes = {
  ResetPassword: {
    template: 'reset.password',
    subject: 'Reset password',
  },
  SignUp: {
    template: 'sign.up',
    subject: 'Your account almost there!',
  },
  InitInNewHub: {
    template: 'init.in.new.hub',
    subject: 'Controller activation',
  },
};
