import { existsSync } from 'fs';
import { config } from 'dotenv';
import { join, resolve } from 'path';
(() => {
  const dotEnvFilePath = join(process.cwd(), '.env');
  const PATH_BOOTSTRAP_CLI_FILE = 'src/cli';

  if (existsSync(dotEnvFilePath)) {
    config({ path: dotEnvFilePath });
  }
  const env = process.env.NODE_ENV;
  const languageType = env === 'production' ? 'js' : 'ts';
  require(resolve(__dirname, `${PATH_BOOTSTRAP_CLI_FILE}.${languageType}`));
})();
