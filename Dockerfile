FROM brainbeanapps/nodejs-build-environment:latest as builder
COPY --chown=user:user . ./project/
WORKDIR project
RUN yarn install
RUN yarn run build

FROM node:10-slim
WORKDIR /opt/api
COPY --from=builder /home/user/project/build/ ./dist/
COPY entrypoint.sh package.json yarn.lock assets ./
RUN yarn install --only=production
CMD [ "./entrypoint.sh" ]
