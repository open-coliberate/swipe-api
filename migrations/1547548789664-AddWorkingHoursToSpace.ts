import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddWorkingHoursToSpace1547548789664 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "spaces" ADD "workingHours" json NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "spaces" DROP COLUMN "workingHours"`);
  }
}
