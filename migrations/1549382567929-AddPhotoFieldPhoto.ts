import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPhotoFieldPhoto1549382567929 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" ADD "photo" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "photo"`);
  }
}
