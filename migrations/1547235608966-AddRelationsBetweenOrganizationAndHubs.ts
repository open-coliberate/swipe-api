import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRelationsBetweenOrganizationAndHubs1547235608966
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "hubs" DROP CONSTRAINT "FK_6d261034abd6f96a727248cef37"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ALTER COLUMN "organizationId" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" DROP CONSTRAINT "REL_6d261034abd6f96a727248cef3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ADD CONSTRAINT "FK_6d261034abd6f96a727248cef37" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "hubs" DROP CONSTRAINT "FK_6d261034abd6f96a727248cef37"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ADD CONSTRAINT "REL_6d261034abd6f96a727248cef3" UNIQUE ("organizationId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ALTER COLUMN "organizationId" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ADD CONSTRAINT "FK_6d261034abd6f96a727248cef37" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
