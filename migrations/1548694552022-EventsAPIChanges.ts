import { MigrationInterface, QueryRunner } from 'typeorm';

export class EventsAPIChanges1548694552022 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "events" ADD "title" character varying`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3fe5f2012780b54e19b991247b" ON "events" USING gin("timeSlot") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_3fe5f2012780b54e19b991247b"`);
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "title"`);
    await queryRunner.query(
      `CREATE INDEX "idx_timeslot" ON "events" ("timeslot") `,
    );
  }
}
