import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingAPISchemaChanges1548409954824
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "buddies"`);
    await queryRunner.query(`ALTER TABLE "bookings" ADD "buddyId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD CONSTRAINT "UQ_3f39f5c4d3b5b7dffa5e3d71ef6" UNIQUE ("buddyId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP COLUMN "eventTimeSlot"`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "eventTimeSlot" jsonb NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "workingHours"`);
    await queryRunner.query(`ALTER TABLE "users" ADD "workingHours" jsonb`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD CONSTRAINT "FK_3f39f5c4d3b5b7dffa5e3d71ef6" FOREIGN KEY ("buddyId") REFERENCES "users"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "FK_3f39f5c4d3b5b7dffa5e3d71ef6"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "workingHours"`);
    await queryRunner.query(`ALTER TABLE "users" ADD "workingHours" json`);
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP COLUMN "eventTimeSlot"`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "eventTimeSlot" json NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "UQ_3f39f5c4d3b5b7dffa5e3d71ef6"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "buddyId"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "buddies" json NOT NULL DEFAULT '[]'`,
    );
  }
}
