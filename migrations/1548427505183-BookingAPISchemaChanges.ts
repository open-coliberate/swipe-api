import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingAPISchemaChanges1548427505183
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE INDEX "IDX_547499af8fa26537bb6ca29f3a" ON "bookings" ("eventType") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_547499af8fa26537bb6ca29f3a"`);
  }
}
