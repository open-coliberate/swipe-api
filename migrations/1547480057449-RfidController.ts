import { MigrationInterface, QueryRunner } from 'typeorm';

export class RfidController1547480057449 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "rfid_controllers" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "tokenId" uuid, CONSTRAINT "REL_e271b96c59c5e644bf024d5740" UNIQUE ("tokenId"), CONSTRAINT "PK_d886c9d0e26a82e926f702fcb78" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ADD CONSTRAINT "FK_e271b96c59c5e644bf024d57401" FOREIGN KEY ("tokenId") REFERENCES "tokens"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP CONSTRAINT "FK_e271b96c59c5e644bf024d57401"`,
    );
    await queryRunner.query(`DROP TABLE "rfid_controllers"`);
  }
}
