import { MigrationInterface, QueryRunner } from 'typeorm';

export class EventsAPIProgress1548691088530 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE INDEX "IDX_3aea230d5fdbdc53ce3bf9091e" ON "events" ("eventDate") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_3aea230d5fdbdc53ce3bf9091e"`);
  }
}
