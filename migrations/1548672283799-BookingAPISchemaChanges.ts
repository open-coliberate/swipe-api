import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingAPISchemaChanges1548672283799
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "FK_74215391af3858ab347890c3287"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "spaceId"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "spaceId" uuid NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD CONSTRAINT "FK_74215391af3858ab347890c3287" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
