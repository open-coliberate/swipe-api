import { MigrationInterface, QueryRunner } from 'typeorm';

export class SpaceAPIHasCreated1547483158063 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "spaces" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "description" text, "hubId" uuid, CONSTRAINT "PK_dbe542974aca57afcb60709d4c8" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "tools" DROP COLUMN "spaceId"`);
    await queryRunner.query(`ALTER TABLE "tools" ADD "spaceId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "hubs" ALTER COLUMN "address" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" ADD CONSTRAINT "FK_266dc2707ba1d447131cc8f6031" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "spaces" ADD CONSTRAINT "FK_73c2330764e0dd46944cd7d6745" FOREIGN KEY ("hubId") REFERENCES "hubs"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "spaces" DROP CONSTRAINT "FK_73c2330764e0dd46944cd7d6745"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" DROP CONSTRAINT "FK_266dc2707ba1d447131cc8f6031"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ALTER COLUMN "address" SET NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "tools" DROP COLUMN "spaceId"`);
    await queryRunner.query(
      `ALTER TABLE "tools" ADD "spaceId" character varying`,
    );
    await queryRunner.query(`DROP TABLE "spaces"`);
  }
}
