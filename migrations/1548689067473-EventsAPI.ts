import { MigrationInterface, QueryRunner } from 'typeorm';

export class EventsAPI1548689067473 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "events" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "eventDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "timeSlot" jsonb NOT NULL, "recurring" character varying DEFAULT 'one_time', "spaceId" uuid NOT NULL, CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "events_tools" ("eventsId" uuid NOT NULL, "toolsId" uuid NOT NULL, CONSTRAINT "PK_332fe9ea2881258cf7f60d1e537" PRIMARY KEY ("eventsId", "toolsId"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP COLUMN "eventTimeSlot"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "eventDate"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "timeSlot" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "bookingDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "events" ADD CONSTRAINT "FK_eb21277724b8de72ebf8fc5b887" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "events_tools" ADD CONSTRAINT "FK_578323b46a32aac8fdf02cee1f6" FOREIGN KEY ("eventsId") REFERENCES "events"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "events_tools" ADD CONSTRAINT "FK_e7593c462539819ec85f309a038" FOREIGN KEY ("toolsId") REFERENCES "tools"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "events_tools" DROP CONSTRAINT "FK_e7593c462539819ec85f309a038"`,
    );
    await queryRunner.query(
      `ALTER TABLE "events_tools" DROP CONSTRAINT "FK_578323b46a32aac8fdf02cee1f6"`,
    );
    await queryRunner.query(
      `ALTER TABLE "events" DROP CONSTRAINT "FK_eb21277724b8de72ebf8fc5b887"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "bookingDate"`);
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "timeSlot"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "eventDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "eventTimeSlot" jsonb NOT NULL`,
    );
    await queryRunner.query(`DROP TABLE "events_tools"`);
    await queryRunner.query(`DROP TABLE "events"`);
  }
}
