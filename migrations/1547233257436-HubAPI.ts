import { MigrationInterface, QueryRunner } from 'typeorm';

export class HubAPI1547233257436 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "hubs" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "address" character varying NOT NULL, "organizationId" uuid NOT NULL, CONSTRAINT "REL_6d261034abd6f96a727248cef3" UNIQUE ("organizationId"), CONSTRAINT "PK_44b53d1f2b4568b26ce4710b843" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "hubs" ADD CONSTRAINT "FK_6d261034abd6f96a727248cef37" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "hubs" DROP CONSTRAINT "FK_6d261034abd6f96a727248cef37"`,
    );
    await queryRunner.query(`DROP TABLE "hubs"`);
  }
}
