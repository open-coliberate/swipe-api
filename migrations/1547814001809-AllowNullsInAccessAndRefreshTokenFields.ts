import { MigrationInterface, QueryRunner } from 'typeorm';

export class AllowNullsInAccessAndRefreshTokenFields1547814001809
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tokens" ALTER COLUMN "accessToken" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tokens" ALTER COLUMN "refreshToken" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tokens" ALTER COLUMN "refreshToken" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tokens" ALTER COLUMN "accessToken" SET NOT NULL`,
    );
  }
}
