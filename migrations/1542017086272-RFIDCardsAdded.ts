import { MigrationInterface, QueryRunner } from 'typeorm';

export class RFIDCardsAdded1542017086272 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "rfid_cards" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "uid" character varying NOT NULL, CONSTRAINT "PK_e2d634e08e25d31915c4a464657" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "users" ADD "rfidCardId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "UQ_4758068266e48aee37158f8da69" UNIQUE ("rfidCardId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_4758068266e48aee37158f8da69" FOREIGN KEY ("rfidCardId") REFERENCES "rfid_cards"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_4758068266e48aee37158f8da69"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "UQ_4758068266e48aee37158f8da69"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "rfidCardId"`);
    await queryRunner.query(`DROP TABLE "rfid_cards"`);
  }
}
