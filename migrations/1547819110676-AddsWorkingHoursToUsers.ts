import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddsWorkingHoursToUsers1547819110676
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" ADD "workingHours" json`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" DROP COLUMN "workingHours"`,
    );
  }
}
