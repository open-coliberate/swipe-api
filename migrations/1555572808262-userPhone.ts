import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserPhone1555572808262 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" ADD "phone" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" SET DEFAULT null`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKeyExpire" SET DEFAULT null`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKeyExpire" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" DROP DEFAULT`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "phone"`);
  }
}
