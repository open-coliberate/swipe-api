import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingsAPI1548067944434 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "bookings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "eventType" character varying NOT NULL, "eventTimeSlot" json NOT NULL, "eventDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "status" character varying NOT NULL DEFAULT 'pending', "buddies" json NOT NULL DEFAULT '[]', "ownerId" uuid NOT NULL, "spaceId" uuid NOT NULL, CONSTRAINT "PK_bee6805982cc1e248e94ce94957" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "tools_bookings" ("toolsId" uuid NOT NULL, "bookingsId" uuid NOT NULL, CONSTRAINT "PK_1e683364119af6b029c6ceedccb" PRIMARY KEY ("toolsId", "bookingsId"))`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bookings" ADD CONSTRAINT "FK_85001d7223966f1b05d302d7807" FOREIGN KEY ("ownerId") REFERENCES "users"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bookings" ADD CONSTRAINT "FK_74215391af3858ab347890c3287" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools_bookings" ADD CONSTRAINT "FK_1f7e06f6657a7add8f6d0b347d7" FOREIGN KEY ("toolsId") REFERENCES "tools"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools_bookings" ADD CONSTRAINT "FK_69b7b69498571e53538e3454a61" FOREIGN KEY ("bookingsId") REFERENCES "bookings"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tools_bookings" DROP CONSTRAINT "FK_69b7b69498571e53538e3454a61"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools_bookings" DROP CONSTRAINT "FK_1f7e06f6657a7add8f6d0b347d7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "FK_74215391af3858ab347890c3287"`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "FK_85001d7223966f1b05d302d7807"`,
    );
    await queryRunner.query(`DROP TABLE "tools_bookings" CASCADE`);
    await queryRunner.query(`DROP TABLE "bookings" CASCADE`);
  }
}
