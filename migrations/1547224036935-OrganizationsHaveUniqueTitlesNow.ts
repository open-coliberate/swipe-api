import { MigrationInterface, QueryRunner } from 'typeorm';

export class OrganizationsHaveUniqueTitlesNow1547224036935
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "organizations" ADD CONSTRAINT "UQ_0f648c5ba1ac0e8674c053f8fff" UNIQUE ("title")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "organizations" DROP CONSTRAINT "UQ_0f648c5ba1ac0e8674c053f8fff"`,
    );
  }
}
