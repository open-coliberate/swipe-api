import { MigrationInterface, QueryRunner } from 'typeorm';

export class SchemaChanges1548343957552 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" DROP CONSTRAINT "FK_de57723dbb2f134a6dd1eb90499"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" DROP CONSTRAINT "FK_2a32e55cc77e6b21880760477d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_templates" ADD "organizationId" uuid NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_templates" ADD CONSTRAINT "FK_dd9ddf2d132d1ef468b2b783613" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" ADD CONSTRAINT "FK_0e7d6c301a6f94b8beccaff0d42" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" ADD CONSTRAINT "FK_114403bac5b8b221277fbcc59f7" FOREIGN KEY ("toolTemplatesId") REFERENCES "tool_templates"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" DROP CONSTRAINT "FK_114403bac5b8b221277fbcc59f7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" DROP CONSTRAINT "FK_0e7d6c301a6f94b8beccaff0d42"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_templates" DROP CONSTRAINT "FK_dd9ddf2d132d1ef468b2b783613"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_templates" DROP COLUMN "organizationId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" ADD CONSTRAINT "FK_2a32e55cc77e6b21880760477d4" FOREIGN KEY ("toolTemplatesId") REFERENCES "tool_templates"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_tool_templates" ADD CONSTRAINT "FK_de57723dbb2f134a6dd1eb90499" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }
}
