import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingAPISchemaChanges1548671306622
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_547499af8fa26537bb6ca29f3a"`);
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "eventType"`);
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "tools"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "toolId" uuid NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD CONSTRAINT "FK_07963e5a48aa002f91020c2a208" FOREIGN KEY ("toolId") REFERENCES "tools"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP CONSTRAINT "FK_07963e5a48aa002f91020c2a208"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "toolId"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "tools" jsonb NOT NULL DEFAULT '[]'`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "eventType" character varying NOT NULL`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_547499af8fa26537bb6ca29f3a" ON "bookings" ("eventType") `,
    );
  }
}
