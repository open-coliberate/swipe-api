import { MigrationInterface, QueryRunner } from 'typeorm';

export class WokringHours1558014166991 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "working_hours" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "day" smallint NOT NULL, "from" integer NOT NULL, "to" integer NOT NULL, "isWorking" boolean NOT NULL, "spaceId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_5f84d2fa3953367fe9d704d8df6" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "spaces" DROP COLUMN "workingHours"`);
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" SET DEFAULT null`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKeyExpire" SET DEFAULT null`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_578323b46a32aac8fdf02cee1f" ON "events_tools" ("eventsId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e7593c462539819ec85f309a03" ON "events_tools" ("toolsId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_0e7d6c301a6f94b8beccaff0d4" ON "users_tool_templates" ("usersId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_114403bac5b8b221277fbcc59f" ON "users_tool_templates" ("toolTemplatesId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "working_hours" ADD CONSTRAINT "FK_50907b0ac61a99ea4b6632d0db6" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "working_hours" ADD CONSTRAINT "FK_697c910993690b1ee75b4c4c118" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "working_hours" DROP CONSTRAINT "FK_697c910993690b1ee75b4c4c118"`,
    );
    await queryRunner.query(
      `ALTER TABLE "working_hours" DROP CONSTRAINT "FK_50907b0ac61a99ea4b6632d0db6"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_114403bac5b8b221277fbcc59f"`);
    await queryRunner.query(`DROP INDEX "IDX_0e7d6c301a6f94b8beccaff0d4"`);
    await queryRunner.query(`DROP INDEX "IDX_e7593c462539819ec85f309a03"`);
    await queryRunner.query(`DROP INDEX "IDX_578323b46a32aac8fdf02cee1f"`);
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKeyExpire" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" DROP DEFAULT`,
    );
    await queryRunner.query(`ALTER TABLE "spaces" ADD "workingHours" json`);
    await queryRunner.query(`DROP TABLE "working_hours"`);
  }
}
