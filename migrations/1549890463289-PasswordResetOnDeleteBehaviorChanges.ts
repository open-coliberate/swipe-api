import { MigrationInterface, QueryRunner } from 'typeorm';

export class PasswordResetOnDeleteBehaviorChanges1549890463289
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac" FOREIGN KEY ("resetPasswordRequestId") REFERENCES "reset_password_requests"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac" FOREIGN KEY ("resetPasswordRequestId") REFERENCES "reset_password_requests"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
