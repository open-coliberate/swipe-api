import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserProfileAPI1549556519134 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" DROP COLUMN IF EXISTS "workingHours"`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" ADD IF NOT EXISTS "workingHours" jsonb`,
    );
  }
}
