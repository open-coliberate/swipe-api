import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingAPISchemaChanges1548412793702
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bookings" ADD "tools" jsonb NOT NULL DEFAULT '[]'`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools_bookings" DROP CONSTRAINT IF EXISTS "FK_69b7b69498571e53538e3454a61"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools_bookings" DROP CONSTRAINT IF EXISTS "FK_1f7e06f6657a7add8f6d0b347d7"`,
    );
    await queryRunner.query(`DROP TABLE IF EXISTS "tools_bookings" CASCADE`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bookings" DROP COLUMN "tools"`,
    );
  }
}
