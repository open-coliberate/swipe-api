import { MigrationInterface, QueryRunner } from 'typeorm';

export class ControllerInitKey1550833146715 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ADD "initKeyExpire" bigint DEFAULT null`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" SET DEFAULT null`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ALTER COLUMN "initKey" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP COLUMN "initKeyExpire"`,
    );
  }
}
