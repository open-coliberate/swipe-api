import { MigrationInterface, QueryRunner } from 'typeorm';

export class ControllerCode1550157102701 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ADD "initKey" character varying DEFAULT null`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ADD CONSTRAINT "UQ_9a96dddde370a09a7bc9dfa57d8" UNIQUE ("initKey")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP CONSTRAINT "UQ_9a96dddde370a09a7bc9dfa57d8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP COLUMN "initKey"`,
    );
  }
}
