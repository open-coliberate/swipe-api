import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserHasNewRelationTypeToOrganization1547803715513
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" DROP CONSTRAINT "FK_4758068266e48aee37158f8da69"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" DROP COLUMN "shortDescription"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "proficiency"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "customField"`);
    await queryRunner.query(`ALTER TABLE "users" ADD "description" text`);
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" ADD "organizationId" uuid NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" ADD CONSTRAINT "FK_4758068266e48aee37158f8da69" FOREIGN KEY ("rfidCardId") REFERENCES "rfid_cards"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" ADD CONSTRAINT "FK_f3d6aea8fcca58182b2e80ce979" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_f3d6aea8fcca58182b2e80ce979"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_4758068266e48aee37158f8da69"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "organizationId"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "description"`);
    await queryRunner.query(`ALTER TABLE "users" ADD "customField" text`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD "proficiency" character varying`,
    );
    await queryRunner.query(`ALTER TABLE "users" ADD "shortDescription" text`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_4758068266e48aee37158f8da69" FOREIGN KEY ("rfidCardId") REFERENCES "rfid_cards"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
