import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingsAPIChanges1548083793182 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `DROP TABLE IF EXISTS "reset-password-requests" CASCADE`,
    );
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "reset_password_requests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "token" character varying NOT NULL, CONSTRAINT "PK_160a92fb6721b4085807e4936e0" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `DROP TABLE IF EXISTS "reset_password_requests" CASCADE`,
    );
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "reset-password-requests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "token" character varying NOT NULL, CONSTRAINT "PK_160a92fb6721b4085807e4936e0" PRIMARY KEY ("id"))`,
    );
  }
}
