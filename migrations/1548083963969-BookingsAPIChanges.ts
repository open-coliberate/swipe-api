import { MigrationInterface, QueryRunner } from 'typeorm';

export class BookingsAPIChanges1548083963969 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" ADD CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac" FOREIGN KEY ("resetPasswordRequestId") REFERENCES "reset_password_requests"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users" DROP CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac"`,
    );
  }
}
