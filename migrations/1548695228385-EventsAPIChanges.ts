import { MigrationInterface, QueryRunner } from 'typeorm';

export class EventsAPIChanges1548695228385 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_3aea230d5fdbdc53ce3bf9091e"`);
    await queryRunner.query(`DROP INDEX "IDX_3fe5f2012780b54e19b991247b"`);
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "timeSlot"`);
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "bookingDate"`);
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "eventDate"`);
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "timeSlot"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "time_slot" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "booking_date" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "events" ADD "event_date" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "events" ADD "time_slot" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_70ce84abd0c092a6fb21ef11c8" ON "events" ("event_date") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_920790fa8c5a8abf41b6ea4637" ON "events" USING gin ("time_slot") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_920790fa8c5a8abf41b6ea4637"`);
    await queryRunner.query(`DROP INDEX "IDX_70ce84abd0c092a6fb21ef11c8"`);
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "time_slot"`);
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "event_date"`);
    await queryRunner.query(
      `ALTER TABLE "bookings" DROP COLUMN "booking_date"`,
    );
    await queryRunner.query(`ALTER TABLE "bookings" DROP COLUMN "time_slot"`);
    await queryRunner.query(
      `ALTER TABLE "events" ADD "timeslot" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "events" ADD "eventDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "bookingDate" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "bookings" ADD "timeSlot" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3fe5f2012780b54e19b991247b" ON "events" ("timeslot") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3aea230d5fdbdc53ce3bf9091e" ON "events" ("eventDate") `,
    );
  }
}
