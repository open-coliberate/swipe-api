import { MigrationInterface, QueryRunner } from 'typeorm';

export class Tools1547213437398 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "tool-templates" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" text NOT NULL, "description" text, "minWorkTime" integer, "maxWorkTime" integer, "buddyMode" boolean NOT NULL, "availableInWorkingHours" boolean NOT NULL, "requiresTraining" boolean NOT NULL, "sound" boolean NOT NULL, CONSTRAINT "PK_ae2a074e968e7feb0557c3e255c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "tools" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "spaceId" character varying, "templateId" uuid, CONSTRAINT "PK_e23d56734caad471277bad8bf85" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" ADD CONSTRAINT "FK_c3b6d04bfd627da5ab7bfeafb64" FOREIGN KEY ("templateId") REFERENCES "tool-templates"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tools" DROP CONSTRAINT "FK_c3b6d04bfd627da5ab7bfeafb64"`,
    );
    await queryRunner.query(`DROP TABLE "tools"`);
    await queryRunner.query(`DROP TABLE "tool-templates"`);
  }
}
