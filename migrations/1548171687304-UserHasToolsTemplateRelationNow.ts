import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserHasToolsTemplateRelationNow1548171687304
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE IF EXISTS "tool-templates" CASCADE`);

    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "tool_templates" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" text NOT NULL, "description" text, "minWorkTime" integer, "maxWorkTime" integer, "buddyMode" boolean NOT NULL, "availableInWorkingHours" boolean NOT NULL, "requiresTraining" boolean NOT NULL, "sound" boolean NOT NULL, CONSTRAINT "PK_ae2a074e968e7feb0557c3e255c" PRIMARY KEY ("id"))`,
    );

    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "users_tool_templates" ("usersId" uuid NOT NULL, "toolTemplatesId" uuid NOT NULL, CONSTRAINT "PK_766afeb414ee8e5f34ba0f96767" PRIMARY KEY ("usersId", "toolTemplatesId"))`,
    );

    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" DROP COLUMN IF EXISTS "timeOfWorkStart"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" DROP COLUMN IF EXISTS "timeOfWorkEnd"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" ADD IF NOT EXISTS "minWorkTime" integer`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" ADD IF NOT EXISTS "maxWorkTime" integer`,
    );

    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools" DROP CONSTRAINT IF EXISTS "FK_c3b6d04bfd627da5ab7bfeafb64"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools" ADD CONSTRAINT "FK_c3b6d04bfd627da5ab7bfeafb64" FOREIGN KEY ("templateId") REFERENCES "tool_templates"("id") ON DELETE SET NULL`,
    );

    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" DROP CONSTRAINT IF EXISTS "FK_de57723dbb2f134a6dd1eb90499"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" ADD CONSTRAINT "FK_de57723dbb2f134a6dd1eb90499" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE`,
    );

    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" DROP CONSTRAINT IF EXISTS "FK_2a32e55cc77e6b21880760477d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" ADD CONSTRAINT "FK_2a32e55cc77e6b21880760477d4" FOREIGN KEY ("toolTemplatesId") REFERENCES "tool_templates"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" DROP CONSTRAINT "FK_2a32e55cc77e6b21880760477d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "users_tool_templates" DROP CONSTRAINT "FK_de57723dbb2f134a6dd1eb90499"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tools" DROP CONSTRAINT "FK_c3b6d04bfd627da5ab7bfeafb64"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" DROP COLUMN "maxWorkTime"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" DROP COLUMN "minWorkTime"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" ADD "timeOfWorkEnd" integer`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "tool_templates" ADD "timeOfWorkStart" integer`,
    );
    await queryRunner.query(`DROP TABLE "users_tool_templates"`);
  }
}
