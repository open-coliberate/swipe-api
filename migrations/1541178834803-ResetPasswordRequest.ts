import { MigrationInterface, QueryRunner } from 'typeorm';

export class ResetPasswordRequest1541178834803 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "reset-password-requests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "token" character varying NOT NULL, CONSTRAINT "PK_adb52bca5b4120d5d42b78153c3" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "resetPasswordRequestId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "UQ_3c41c5eb950a717eaa1854410ac" UNIQUE ("resetPasswordRequestId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac" FOREIGN KEY ("resetPasswordRequestId") REFERENCES "reset-password-requests"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_3c41c5eb950a717eaa1854410ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "UQ_3c41c5eb950a717eaa1854410ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP COLUMN "resetPasswordRequestId"`,
    );
    await queryRunner.query(`DROP TABLE "reset-password-requests"`);
  }
}
