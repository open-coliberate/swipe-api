import { MigrationInterface, QueryRunner } from 'typeorm';

export class RFIDCardUIDISUnique1542017224908 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_cards" ADD CONSTRAINT "UQ_2f111cab9640fcb56eaaf34a2fa" UNIQUE ("uid")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rfid_cards" DROP CONSTRAINT "UQ_2f111cab9640fcb56eaaf34a2fa"`,
    );
  }
}
