import { MigrationInterface, QueryRunner } from 'typeorm';

export class SpaceMoveToController1548772392930 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tools" DROP CONSTRAINT IF EXISTS "FK_266dc2707ba1d447131cc8f6031"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" RENAME COLUMN "spaceId" TO "controllerId"`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "rfid_controllers" ADD IF NOT EXISTS "spaceId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" ADD CONSTRAINT "FK_4fabf53e39ee0dff1bb86a4f93b" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" ADD CONSTRAINT "FK_6a3b2ff313d2147fdc51e881c03" FOREIGN KEY ("controllerId") REFERENCES "rfid_controllers"("id") ON DELETE SET NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tools" DROP CONSTRAINT IF EXISTS "FK_6a3b2ff313d2147fdc51e881c03"`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP CONSTRAINT IF EXISTS "FK_4fabf53e39ee0dff1bb86a4f93b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "rfid_controllers" DROP COLUMN IF EXISTS "spaceId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" RENAME COLUMN "controllerId" TO "spaceId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tools" ADD CONSTRAINT IF NOT EXISTS "FK_266dc2707ba1d447131cc8f6031" FOREIGN KEY ("spaceId") REFERENCES "spaces"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
