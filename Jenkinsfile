#!/usr/bin/env groovy

pipeline {
  agent any

  environment {
    IMAGE_NAME = 'coliberate/api'
    TEMPORARY_IMAGE_NAME = "${env.JOB_NAME.toLowerCase()}"
    SANITIZED_BRANCH_NAME = env.BRANCH_NAME.replaceAll(/[^\w\d]/, '-').replaceAll(/-{2,}/, '-')
    IMAGE_TAG_NAME = "${SANITIZED_BRANCH_NAME}.${env.BUILD_ID}"
    PROJECT_JOB_NAME = "${env.JOB_NAME}"
    TARGET_ENV = ''
  }

  stages {
    stage('Checkout') {
      steps {
        checkout scm
      }
    }

    stage('Configure') {
      steps {
        script {
          PROJECT_JOB_NAME = env.JOB_NAME.replace("/${env.JOB_BASE_NAME}", '')
          if (env.BRANCH_NAME ==~ /^release\/v\d+(.\d+(.\d)?)?$/) {
            TARGET_ENV = 'production'
          } else if (env.BRANCH_NAME ==~ /^rc\/v\d+(.\d+(.\d)?)?-\d+$/) {
            TARGET_ENV = 'staging'
          } else {
            TARGET_ENV = 'dev'
          }
          SANITIZED_PROJECT_JOB_NAME = PROJECT_JOB_NAME.replaceAll(/[^\w\d]/, '-').replaceAll(/-{2,}/, '-')
          TEMPORARY_IMAGE_NAME = "${SANITIZED_PROJECT_JOB_NAME.toLowerCase()}"
        }
      }
    }

    stage('Assemble') {
      steps {
        script {
          docker.build("${TEMPORARY_IMAGE_NAME}:${IMAGE_TAG_NAME}", '--pull .')
        }
      }
    }

    stage('Archive') {
      steps {
        script {
          def awsEcrLogin = ''
          withAWS(region:'us-east-1', credentials:'aws-ecr-buildbot') {
            awsEcrLogin = ecrLogin()
          }

          sh "${awsEcrLogin}"

          def account = '955707465911.dkr.ecr.us-east-1.amazonaws.com/'
          docker.script.sh "docker tag ${TEMPORARY_IMAGE_NAME}:${IMAGE_TAG_NAME} ${account}${IMAGE_NAME}:${IMAGE_TAG_NAME}"

          def image = docker.image("${account}${IMAGE_NAME}:${IMAGE_TAG_NAME}")
          image.push()
          image.push("${SANITIZED_BRANCH_NAME}.latest")
        }
      }
    }

    stage('Deployment') {
      when {
        allOf {
          anyOf {
            branch 'master'
          }
          expression {
            currentBuild.result == null || currentBuild.result == 'SUCCESS'
          }
        }
      }
      steps {
        build job: '../../BBA/heroku-docker-deploying', parameters: [
          string(name: 'repository', value: "${IMAGE_NAME}"),
          string(name: 'tag', value: "${IMAGE_TAG_NAME}"),
          string(name: 'herokuApp', value: 'dev-brainbean-api'),
          string(name: 'herokuProcessType', value: 'web')
        ]
      }
    }
  }
}
