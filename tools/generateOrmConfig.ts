import { join } from 'path';
import { template } from 'lodash';
import { promisify } from 'util';
import { writeFile, existsSync, unlink } from 'fs';
import { createLogger, format, transports, Logger } from 'winston';
import { config } from 'dotenv';
const logger: Logger = createLogger({
  level: 'info',
  format: format.simple(),
  transports: [new transports.Console()],
});
const generateOrmConfig = async (): Promise<void> => {
  const DATABASE_TYPE = 'postgres';
  const ORM_CONFIG_FILE: string = 'ormconfig.json';
  const ROOT_PATH: string = process.cwd();
  const pathToWriteOrmConfig = join(ROOT_PATH, ORM_CONFIG_FILE);
  const writeFileAsync = promisify(writeFile);
  const unlinkAsync = promisify(unlink);
  try {
    if (existsSync(join(ROOT_PATH, '.env'))) {
      config();
    }
    const env = process.env.APP_ENV || process.env.NODE_ENV;
    const configTemplate = `
    {
        "type": "<%= type %>",
        "host": "<%= host %>",
        "port": <%= port %>,
        "username": "<%= username %>",
        "password": "<%= password %>",
        "database": "<%= database %>",
        "entities": [<%= entities %>],
        "subscribers": ["<%= subscribers %>"],
        "migrationsTableName": "<%= migrationsTableName %>",
        "migrations": ["<%= migrations %>"],
        "cli": {
            "migrationsDir": "<%= migrationsFolder %>"
        },
        "logging": "${env === 'development' ? 'all' : ''}"
    }`;
    logger.info(`Current env is: ${env}`);
    const buildExtension = env === 'production' ? 'js' : 'ts';
    const typeOrmConfigObject = {
      type: DATABASE_TYPE,
      host: process.env.DATABASE_HOST,
      port: process.env.DATABASE_PORT,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: `"./src/**/**.entity.${buildExtension}"`,
      subscribers: `./src/**/**.subscriber.${buildExtension}`,
      migrationsTableName: 'migrations',
      migrations: `./migrations/*.${buildExtension}`,
      migrationsFolder: 'migrations',
    };
    const compiledTemplate = template(configTemplate)(typeOrmConfigObject);
    if (existsSync(pathToWriteOrmConfig)) {
      logger.warn('Orm config file already exists - lets recreate them');
      await unlinkAsync(pathToWriteOrmConfig);
    }
    await writeFileAsync(pathToWriteOrmConfig, compiledTemplate);
    logger.info(`Orm config has been created in: ${pathToWriteOrmConfig}`);
  } catch (e) {
    logger.error('Error occurs during ormconfig file creation');
    throw e;
  }
};
generateOrmConfig();
