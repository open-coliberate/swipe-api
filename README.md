# Nest.js, Typescript based project by Brainbean Apps

Coliberate API backend

## Endpoints

### `GET /ping`

Replies with:
```
{
    "ping": "pong",
    "timestamp": 1540558982919
}
```

## CLI 
Run existed script with command

In develop mode
``` $ yarn run <command name> <attributes> ```

In production mode 
``` dist$ node cli.js <command name> <attributes> ```

Allowed commands:
- `seeder:all <name>` seeds database with test data. Command creates Organization, Hub, Space and default Controller and binds them.
After creates ToolTemplate and 2 tools attached to it. 
Then creates user, activate him and make event and booking to one of created tool. 
In the end command returns user email and password in output. 
You can use it to log in as new user. 

## References

* [Nest.JS Starter by Nest](https://github.com/nestjs/typescript-starter)
